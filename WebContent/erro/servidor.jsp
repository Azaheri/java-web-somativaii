<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%-- URLs --%>
<c:url value="../assets/css/" var="css" />
<%-- URLs --%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Erro de servidor</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" media="screen"
	href="../assets/css/main.css" />
</head>
<body style="margin: 0px; height: 100vh; background-color: #faebd781;">
	<c:import url="../WEB-INF/templates/navbar.jsp" />
	<h1 class="fw-l alg-text-centro m-t-25" style="margin: 0px; font-weight: lighter; text-align: center; font-size: 64px; margin-top: 20vh;">500 - Erro de servidor</h1>
	<h3 class="fw-l alg-text-centro m-t-10" style="margin: 0px; font-weight: lighter; text-align: center; font-size: 48px; margin-top: 10vh;">Busque ajuda com seu programador de estimação mais próximo.</h3>
</body>
</html>