<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%-- URLs --%>
<c:url value="/app/" var="home"/>
<c:url value="/assets/css/" var="css"/>
<c:url value="/assets/imagens/" var="img"/>
<c:url value="/app/usuariod" var="usuariod"/>
<c:url value="/app/ambiente" var="ambiente"/>
<c:url value="/app/patrimonio" var="patrimonio"/>
<c:url value="/app/categoria" var="categoria"/>
<c:url value="/app/item" var="itempatrimonio"/>
<c:url value="/app/movimentacao" var="movimentacao"/>
<c:url value="/app/adm/usuario" var="editUsuario"/>
<c:url value="/app/usuario/alterarSenha" var="editSenhaUsuario"/>
<c:url value="/app/adm/usuario/novo" var="addUsuario"/>
<c:url value="/app/patrimonio/lista" var="listapatrimonio"/>
<c:url value="/app/adm/patrimonio/novo" var="addpatrimonio"/>
<c:url value="/app/ambiente/lista" var="listaAmbiente"/>
<c:url value="/app/adm/ambiente/nova" var="addAmbiente"/>
<c:url value="/app/itempatrimonio/lista" var="listaitempatrimonio"/>
<c:url value="/app/itempatrimonio/novo" var="additempatrimonio"/>
<c:url value="/app/categoria/lista" var="listaCategoria"/>
<c:url value="/app/adm/categoria/nova" var="addCategoria"/>
<c:url value="/app/movimentacao/lista" var="listaMovimentacao"/>
<c:url value="/app/movimentacao/novo" var="addMovimentacao"/>
<c:url value="/sair" var="logout"/>
<%-- URLs --%>

<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8" />
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" media="screen" href="${css}nav.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="${css}fontawesome-all.min.css" />
		<link rel="icon" href="${img}logo-senaipkeeper.svg" type="image/x-icon" />
	</head>
	<body>
		<ul>
			<li><a href="${home}">Home</a></li>
		  	<li class="dropdown">
			    <a href="javascript:void(0)" class="dropbtn">Usuário</a>
			    <div class="dropdown-content">
			    	<a href="${editSenhaUsuario}">Alterar senha</a>
			    	<c:if test="${usuarioAutenticado.tipo == 'ADMINISTRADOR'}">
				    	<a href="${addUsuario}">Adicionar usuário</a>
				    	<a href="${editUsuario}">Listar usuários</a>
			    	</c:if>
			    </div>
		  	</li>
		  	<li class="dropdown">
			    <a href="javascript:void(0)" class="dropbtn">Ambiente</a>
			    <div class="dropdown-content">
			    	<a href="${listaAmbiente}">Lista de ambientes</a>
			    	<c:if test="${usuarioAutenticado.tipo == 'ADMINISTRADOR'}">
				    	<a href="${addAmbiente}">Adicionar ambiente</a>
			    	</c:if>
			    </div>
		  	</li>
		  	<li class="dropdown">
			    <a href="javascript:void(0)" class="dropbtn">Categoria</a>
			    <div class="dropdown-content">
			    	<a href="${listaCategoria}">Lista de categorias</a>
			    	<c:if test="${usuarioAutenticado.tipo == 'ADMINISTRADOR'}">
				    	<a href="${addCategoria}">Adicionar Categoria</a>
			    	</c:if>
			    </div>
		  	</li>
		  	<li class="dropdown">
			    <a href="javascript:void(0)" class="dropbtn">Patrimônio</a>
			    <div class="dropdown-content">
			    	<a href="${listapatrimonio}">Lista de patrimônios</a>
			    	<c:if test="${usuarioAutenticado.tipo == 'ADMINISTRADOR'}">
				    	<a href="${addpatrimonio}">Adicionar patrimônio</a>
			    	</c:if>
			    </div>
		  	</li>
			<li class="dropdown">
			    <a href="javascript:void(0)" class="dropbtn">Itens de um patrimônio</a>
			    <div class="dropdown-content">
			    	<a href="${listaitempatrimonio}">Lista de itens</a>
				    <a href="${additempatrimonio}">Adicionar item</a>
			    </div>
			</li>
		  	
			<li><a href="${listaMovimentacao}">Lista de Movimentações</a></li>
			<li><a href="${logout}">Logout</a></li>
		</ul>
	</body>
</html>