CREATE DATABASE  IF NOT EXISTS `senaipkeeper` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `senaipkeeper`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: senaipkeeper
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ambiente`
--

DROP TABLE IF EXISTS `ambiente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ambiente` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nome` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_slgf5nwbx260gec72ysmakj3n` (`nome`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ambiente`
--

LOCK TABLES `ambiente` WRITE;
/*!40000 ALTER TABLE `ambiente` DISABLE KEYS */;
INSERT INTO `ambiente` VALUES (4,'Co-working'),(3,'Coordenação'),(2,'Diretoria'),(5,'Refeitório'),(1,'Sala 11'),(6,'Tesouraria');
/*!40000 ALTER TABLE `ambiente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_prx5elpv558ah8pk8x18u56yc` (`nome`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (1,'Cadeira'),(4,'Computador'),(3,'Monitor'),(6,'Mouse'),(2,'Talher'),(5,'Teclado');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_patrimonio`
--

DROP TABLE IF EXISTS `item_patrimonio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_patrimonio` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ultimaMovimentacao` datetime DEFAULT NULL,
  `ambiente_id` bigint(20) NOT NULL,
  `patrimonio_id` bigint(20) NOT NULL,
  `usuario_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7tl8lb5jv62asfe2m8pdornh2` (`ambiente_id`),
  KEY `FKl1mkuwd50kqrhlkq3u1lnai5f` (`patrimonio_id`),
  KEY `FK3vq1vj9waya8a6emyvhjovarj` (`usuario_id`),
  CONSTRAINT `FK3vq1vj9waya8a6emyvhjovarj` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`),
  CONSTRAINT `FK7tl8lb5jv62asfe2m8pdornh2` FOREIGN KEY (`ambiente_id`) REFERENCES `ambiente` (`id`),
  CONSTRAINT `FKl1mkuwd50kqrhlkq3u1lnai5f` FOREIGN KEY (`patrimonio_id`) REFERENCES `patrimonio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_patrimonio`
--

LOCK TABLES `item_patrimonio` WRITE;
/*!40000 ALTER TABLE `item_patrimonio` DISABLE KEYS */;
INSERT INTO `item_patrimonio` VALUES (1,NULL,3,1,1),(2,NULL,1,2,1),(3,NULL,1,8,1),(4,NULL,2,3,1),(5,NULL,4,4,1),(6,'2018-06-08 17:50:53',2,5,1),(7,NULL,5,6,1),(8,NULL,4,6,1),(9,'2018-06-07 16:43:48',4,9,1),(10,'2018-06-07 16:44:16',5,10,1),(11,NULL,5,11,1);
/*!40000 ALTER TABLE `item_patrimonio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimentacao`
--

DROP TABLE IF EXISTS `movimentacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimentacao` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dataMovimentacao` datetime NOT NULL,
  `ambiente_destino_id` bigint(20) NOT NULL,
  `ambiente_origem_id` bigint(20) NOT NULL,
  `item_patrimonio_id` bigint(20) NOT NULL,
  `usuario_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKd1wit05hwgpeujsjjpbayiats` (`ambiente_destino_id`),
  KEY `FKhs9fd6c8prhbwwy30gyyvtjw4` (`ambiente_origem_id`),
  KEY `FKfvhjx0r9drbf1gb4nn3d748wo` (`item_patrimonio_id`),
  KEY `FKm285fyfcychfcyeunh0vcr3i6` (`usuario_id`),
  CONSTRAINT `FKd1wit05hwgpeujsjjpbayiats` FOREIGN KEY (`ambiente_destino_id`) REFERENCES `ambiente` (`id`),
  CONSTRAINT `FKfvhjx0r9drbf1gb4nn3d748wo` FOREIGN KEY (`item_patrimonio_id`) REFERENCES `item_patrimonio` (`id`),
  CONSTRAINT `FKhs9fd6c8prhbwwy30gyyvtjw4` FOREIGN KEY (`ambiente_origem_id`) REFERENCES `ambiente` (`id`),
  CONSTRAINT `FKm285fyfcychfcyeunh0vcr3i6` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimentacao`
--

LOCK TABLES `movimentacao` WRITE;
/*!40000 ALTER TABLE `movimentacao` DISABLE KEYS */;
INSERT INTO `movimentacao` VALUES (1,'2018-06-07 16:43:48',4,3,9,1),(2,'2018-06-07 16:44:16',1,4,10,1),(3,'2018-06-08 17:15:26',1,1,10,1),(4,'2018-06-08 17:17:12',5,1,10,1),(5,'2018-06-08 17:17:46',3,2,1,1),(6,'2018-06-08 17:19:31',1,5,6,1),(7,'2018-06-08 17:24:32',2,1,6,1),(8,'2018-06-08 17:24:39',1,2,6,1),(9,'2018-06-08 17:50:53',2,1,6,1);
/*!40000 ALTER TABLE `movimentacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patrimonio`
--

DROP TABLE IF EXISTS `patrimonio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patrimonio` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dataCadastro` datetime DEFAULT NULL,
  `nome` varchar(40) NOT NULL,
  `categoria_id` bigint(20) NOT NULL,
  `usuario_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_pjmu1upl76pfuudj5lh6ru46s` (`nome`),
  KEY `FK5dli6ws53e5ihft98b10omnnk` (`categoria_id`),
  KEY `FK9au8amt1c67whsioht1ehn3pu` (`usuario_id`),
  CONSTRAINT `FK5dli6ws53e5ihft98b10omnnk` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`),
  CONSTRAINT `FK9au8amt1c67whsioht1ehn3pu` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patrimonio`
--

LOCK TABLES `patrimonio` WRITE;
/*!40000 ALTER TABLE `patrimonio` DISABLE KEYS */;
INSERT INTO `patrimonio` VALUES (1,'2018-06-07 16:39:22','Preta Confortável',1,1),(2,'2018-06-07 16:39:29','Azul Escrota',1,1),(3,'2018-06-07 16:39:33','Reclinável',1,1),(4,'2018-06-07 16:39:36','Puff',1,1),(5,'2018-06-07 16:39:38','Banqueta',1,1),(6,'2018-06-07 16:39:42','Quatro Pernas Azul',1,1),(7,'2018-06-07 16:41:16','LG Ultra-wide',3,1),(8,'2018-06-07 16:41:22','Oki',6,1),(9,'2018-06-07 16:41:27','Vaio',4,1),(10,'2018-06-07 16:41:33','Corsair',5,1),(11,'2018-06-07 16:41:42','Faca de Plástico',2,1);
/*!40000 ALTER TABLE `patrimonio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `nome` varchar(20) NOT NULL,
  `senha` varchar(32) NOT NULL,
  `sobrenome` varchar(40) NOT NULL,
  `tipo` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_5171l57faosmj8myawaucatdw` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'admin@email.com','Administrador','0192023a7bbd73250516f069df18b500','Adm',0);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'senaipkeeper'
--

--
-- Dumping routines for database 'senaipkeeper'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-08 16:21:52
