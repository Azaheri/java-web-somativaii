DROP DATABASE IF EXISTS PatrimonioIANES;
CREATE DATABASE PatrimonioIANES;
USE PatrimonioIANES;

CREATE TABLE ambiente (
id_ambiente BIGINT AUTO_INCREMENT PRIMARY KEY,
nome VARCHAR(40) UNIQUE NOT NULL
);

CREATE TABLE tipo_usuario (
id_tipo BIGINT AUTO_INCREMENT PRIMARY KEY,
tipo VARCHAR(20) NOT NULL
);

CREATE TABLE categoria_patrimonio (
id_categoria BIGINT AUTO_INCREMENT PRIMARY KEY,
nome VARCHAR(30) NOT NULL
);

CREATE TABLE usuario (
id_usuario BIGINT AUTO_INCREMENT PRIMARY KEY,
sobrenome VARCHAR(40) NOT NULL,
nome VARCHAR(20) NOT NULL,
email VARCHAR(255) UNIQUE NOT NULL,
senha VARCHAR(32) NOT NULL,
id_tipo BIGINT NOT NULL
);

CREATE TABLE patrimonio (
id_patrimonio BIGINT AUTO_INCREMENT PRIMARY KEY,
nome VARCHAR(40) NOT NULL,
data_cadastro DATE NOT NULL,
id_categoria BIGINT NOT NULL,
id_usuario BIGINT NOT NULL,
FOREIGN KEY(id_usuario) REFERENCES usuario (id_usuario) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE item_do_patrimonio (
id_item BIGINT AUTO_INCREMENT PRIMARY KEY,
id_patrimonio BIGINT NOT NULL,
id_ambiente BIGINT NOT NULL,
id_usuario BIGINT NOT NULL,
FOREIGN KEY(id_patrimonio) REFERENCES patrimonio (id_patrimonio),
FOREIGN KEY(id_ambiente) REFERENCES ambiente (id_ambiente),
FOREIGN KEY(id_usuario) REFERENCES usuario (id_usuario) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE movimentacao (
id_movimentacao BIGINT AUTO_INCREMENT PRIMARY KEY,
id_ambiente1 BIGINT NOT NULL,
id_ambiente2 BIGINT NOT NULL,
id_usuario BIGINT NOT NULL,
id_item BIGINT NOT NULL,
FOREIGN KEY(id_item) REFERENCES item_do_patrimonio (id_item),
FOREIGN KEY(id_ambiente1) REFERENCES ambiente (id_ambiente),
FOREIGN KEY(id_ambiente2) REFERENCES ambiente (id_ambiente),
FOREIGN KEY(id_usuario) REFERENCES usuario (id_usuario) ON DELETE CASCADE ON UPDATE CASCADE
);

ALTER TABLE usuario ADD FOREIGN KEY(id_tipo) REFERENCES tipo_usuario (id_tipo);
ALTER TABLE patrimonio ADD FOREIGN KEY(id_categoria) REFERENCES categoria_patrimonio (id_categoria);

-- Para adicionar tipos de usuários
/*INSERT INTO tipo_usuario(tipo) VALUE (?);*/
-- Para adicionar usuários
/*INSERT INTO usuario(nome, sobrenome, email, senha, id_tipo) VALUES (?, ?, ?, ?, ?);*/
-- Para adicionar ambientes
/*INSERT INTO ambiente(nome) VALUE (?);*/
-- Para adicionar categorias de patrimonio
/*INSERT INTO categoria_patrimonio(nome) VALUE (?);*/
-- Para adicionar patrimonios
/*INSERT INTO patrimonio(nome, data_cadastro, id_categoria, id_usuario) VALUES (?, ?, ?, ?);*/
-- Para adicionar itens ao patrimonio
/*INSERT INTO item_do_patrimonio(id_patrimonio, id_ambiente, id_usuario) VALUES (?,  ?, ?);*/
-- Para adicionar movimentações
/*INSERT INTO movimentacao(id_ambiente1, id_ambiente2, id_usuario, id_item) VALUES (?, ?, ?, ?);*/
-- Para autenticar usuários
/*SELECT * FROM usuario WHERE email = ? AND senha = ?;*/
-- Para alterar usuários
/*UPDATE usuario SET nome = ?, sobrenome = ?, email = ?, senha = ?, id_tipo = WHERE id_usuario = ?;*/
-- Para alterar dados (Nome e sobrenome)
/*UPDATE usuario SET nome = ?, sobrenome = WHERE id_usuario = ?;*/
-- Para alterar senha
/*UPDATE usuario SET senha = WHERE id_usuario = ?;*/
-- Para deletar usuários
/*DELETE FROM usuario WHERE id_usuario = ?;*/
-- Para listar usuários
/*SELECT nome, sobrenome, email, id_tipo FROM usuario;*/
-- Para listar patrimonios
/*SELECT * FROM patrimonio;*/
-- Para alterar patrimonios
/*UPDATE patrimonio SET nome = ?, data_cadastro = ?, id_categoria = ?, id_usuario = WHERE id_patrimonio = ?;*/
-- Para deletar patrimonios
/*DELETE FROM patrimonio WHERE id_patrimonio = ?;*/
-- Para listar ambientes
/*SELECT * FROM ambiente;*/
-- Para alterar ambientes
/*UPDATE ambiente SET nome = WHERE id_ambiente = ?;*/
-- Para deletar ambientes
/*DELETE FROM ambiente WHERE id_ambiente = ?;*/
-- Deletar itens do patrimonio
/*DELETE FROM item_do_patrimonio WHERE id_item = ?;*/
-- Visualizar movimentações
/*SELECT * FROM movimentacao WHERE id_item = ?;*/

-- Cadastro de tipos de usuário
INSERT INTO tipo_usuario(tipo) VALUE ('Comum');
INSERT INTO tipo_usuario(tipo) VALUE ('Administrador');

-- Cadastro de usuário
INSERT INTO usuario(nome, sobrenome, email, senha, id_tipo) VALUES ('Matheus', 'José', 'pwvmat@gmail.com', 'senha123', 2);
INSERT INTO usuario(nome, sobrenome, email, senha, id_tipo) VALUES ('Gabriel', 'Quintas', 'gabriel@email.com', 'senha132', 1);
INSERT INTO usuario(nome, sobrenome, email, senha, id_tipo) VALUES ('Gustavo', 'Donegá', 'gustavo@email.com', 'senha321', 1);
INSERT INTO usuario(nome, sobrenome, email, senha, id_tipo) VALUES ('Teste', 'Teste', 'email@email.com', 'senha312', 1);
INSERT INTO usuario(nome, sobrenome, email, senha, id_tipo) VALUES ('vao', 'foi', 'vai@email.com', 'senha231', 1);
INSERT INTO usuario(nome, sobrenome, email, senha, id_tipo) VALUES ('deleta', 'deleta', 'deleta@email.com', 'senha213', 1);

-- Cadastrar ambiente
INSERT INTO ambiente(nome) VALUE ('Sala 22');
INSERT INTO ambiente(nome) VALUE ('Co-working');
INSERT INTO ambiente(nome) VALUE ('Diretoria');
INSERT INTO ambiente(nome) VALUE ('Ambiente deletado');

-- Cadastro de categoria de patrimônio
INSERT INTO categoria_patrimonio(nome) VALUE ('Monitores');
INSERT INTO categoria_patrimonio(nome) VALUE ('Computadores');
INSERT INTO categoria_patrimonio(nome) VALUE ('Cadeiras');

-- Cadastrar patrimônio (somente administrador)
INSERT INTO patrimonio(nome, data_cadastro, id_categoria, id_usuario) VALUES ('Dell 19', '2018-03-20', 1, 1);
INSERT INTO patrimonio(nome, data_cadastro, id_categoria, id_usuario) VALUES ('Senta Bem ruim', '2018-03-19', 3, 1);
INSERT INTO patrimonio(nome, data_cadastro, id_categoria, id_usuario) VALUES ('Cadeira Preta Boa', '2018-03-20', 3, 1);
INSERT INTO patrimonio(nome, data_cadastro, id_categoria, id_usuario) VALUES ('Dell PC', '2018-03-17', 2, 1);
INSERT INTO patrimonio(nome, data_cadastro, id_categoria, id_usuario) VALUES ('Dell 21', '2018-03-18', 1, 1);

-- Cadastrar item de patrimônio
INSERT INTO item_do_patrimonio(id_patrimonio, id_ambiente, id_usuario) VALUES (1, 1, 1);
INSERT INTO item_do_patrimonio(id_patrimonio, id_ambiente, id_usuario) VALUES (3, 3, 1);
INSERT INTO item_do_patrimonio(id_patrimonio, id_ambiente, id_usuario) VALUES (4, 2, 1);
INSERT INTO item_do_patrimonio(id_patrimonio, id_ambiente, id_usuario) VALUES (5, 2, 1);

-- Realizar movimentação
INSERT INTO movimentacao(id_ambiente1, id_ambiente2, id_usuario, id_item) VALUES (3, 2, 1, 2);
UPDATE item_do_patrimonio SET id_ambiente = 3 WHERE id_item = 2;

-- Autenticação de usuário
SELECT * FROM usuario WHERE email = 'pwvmat@gmail.com' AND senha = 'senha123';

-- Alteração de usuário
UPDATE usuario SET nome = 'mudou', sobrenome = 'mudou', email = 'mudou@email.com', senha = 'mudou', id_tipo = 2 WHERE id_usuario = 4;

-- Alterar dados do usuário autenticado (Nome e sobrenome)
UPDATE usuario SET nome = 'updatou', sobrenome = 'updatou' WHERE id_usuario = 5;

-- Alterar senha
UPDATE usuario SET senha = 'updatou' WHERE id_usuario = 5;

-- Excluir usuário (somente administrador)
DELETE FROM usuario WHERE id_usuario = 6;

-- Listar usuários (no caso listo tudo menos a senha pois é algo que o administrador não necessita saber)
SELECT nome, sobrenome, email, id_tipo FROM usuario;

-- Listar patrimônios
SELECT * FROM patrimonio;

-- Alterar patrimônio
UPDATE patrimonio SET nome = 'Dell 22', data_cadastro = '2018-03-20', id_categoria = 1, id_usuario = 1 WHERE id_patrimonio = 5;

-- Excluir patrimônio
DELETE FROM patrimonio WHERE id_patrimonio = 2;

-- Listar ambiente
SELECT * FROM ambiente;

-- Alterar ambiente
UPDATE ambiente SET nome = 'Escritório' WHERE id_ambiente = 2;

-- Excluir ambiente
DELETE FROM ambiente WHERE id_ambiente = 4;

-- Excluir item de patrimônio
DELETE FROM item_do_patrimonio WHERE id_item = 4;

-- Listar itens de patrimônio
SELECT patrimonio.nome AS 'Patrimônio', categoria_patrimonio.nome AS 'Categoria', ambiente.nome AS 'Ambiente' FROM item_do_patrimonio
JOIN patrimonio ON item_do_patrimonio.id_patrimonio = patrimonio.id_patrimonio
JOIN categoria_patrimonio ON patrimonio.id_categoria = categoria_patrimonio.id_categoria
JOIN ambiente ON item_do_patrimonio.id_ambiente = ambiente.id_ambiente;
	
-- Visualizar movimentações de um determinado item
SELECT * FROM movimentacao WHERE id_item = 2;

SELECT * FROM item_do_patrimonio;
SELECT * FROM ambiente;
select * FROM patrimonio;

#############################################################################################################################################################
# 														ATIVIDADE DO DIA 22 DE MAIO DE 2018																	#
#############################################################################################################################################################
#																																							#
# 1º - Obter a quantidade de itens de patrimônio em determinado ambiente. 																					#
#																																							#
SELECT a.nome, COUNT(*) AS 'Qtd.' FROM item_do_patrimonio AS i																								#
JOIN ambiente AS a ON i.id_ambiente = a.id_ambiente																											#
WHERE a.id_ambiente = 1;																																	#
#																																							#
# 2º Obter a quantidade de itens de patrimônio em todos os ambientes. 																						#
#																																							#
SELECT a.nome, COUNT(*) AS 'Qtd.' FROM item_do_patrimonio AS i JOIN																							#
ambiente AS a ON i.id_ambiente = a.id_ambiente GROUP BY a.id_ambiente;																						#
#																																							#
# 3º Contar os patrimônios para cada categoria de patrimônio. 																								#
#																																							#
SELECT p.nome, COUNT(*) AS 'Qtd.' FROM patrimonio AS p																										#
JOIN categoria_patrimonio AS c ON p.id_categoria = c.id_categoria																							#
GROUP BY c.id_categoria;																																	#
#																																							#
# 4º Obter o ambiente que possui a maior quantidade de itens de patrimônio.																					#
#																																							#
SELECT a.nome AS 'Ambiente', MAX(a.id_ambiente) as mi FROM item_do_patrimonio AS i																			#
JOIN ambiente AS a ON i.id_ambiente = a.id_ambiente GROUP BY a.id_ambiente;																					#
#																																							#
#############################################################################################################################################################
