<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%-- URLs --%>
<c:url value="/app/movimentacao/lista" var="listamovimentacao"/>
<c:url value="/app/movimentacao/novo" var="addmovimentacao"/>
<c:url value="/app" var="home"/>
<c:url value="/assets/css/" var="css"/>
<%-- URLs --%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SPKeeper - Listar Movimentações</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="${css}main.css" />
</head>
    <body>
    <c:import url="../../templates/navbar.jsp"/>
        <main>
        <h2 class="fw-l alg-text-centro">Lista das movimentações realizadas</h2>
            <div>
            	<div>
            		<table>
            			<thead>
            				<tr>
								<th>Id de registro</th>
							    <th>Origem</th>
							    <th>Destino</th>
							    <th>Patrimonio movimentado</th>
							    <th>Item movimentado</th>
							    <th>Responsável</th>
							    <th>Data da movimentação</th>
            				</tr>
            			</thead>
            			<tbody>
            				<c:forEach items="${movimentacoes}" var="movimentacao">
							  <tr>
							    <td>${movimentacao.id}</td>
							    <td>${movimentacao.ambiente_origem.nome}</td>
							    <td>${movimentacao.ambiente_destino.nome}</td>
							    <td>${movimentacao.itemPatrimonio.patrimonio.nome}</td>
							    <td>${movimentacao.itemPatrimonio.id}</td>
							    <td>${movimentacao.usuario.nome} ${movimentacao.usuario.sobrenome}</td>
							    <td><fmt:formatDate value="${movimentacao.dataMovimentacao}" pattern="dd/MM/yyyy kk:mm:ss"/></td>
							  </tr>
            				</c:forEach>
            			</tbody>
            		</table>
            	</div>
            </div>
        </main>
    </body>
</html>