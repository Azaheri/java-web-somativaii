<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%-- URLs --%>
<c:url value="/app/movimentacao/lista" var="listamovimentacao" />
<c:url value="/app/movimentacao/novo" var="addmovimentacao" />
<c:url value="/app/movimentacao/salvar" var="salvarmovimentacao" />
<c:url value="/app" var="home" />
<c:url value="/assets/css/" var="css" />
<%-- URLs --%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>SPKeeper - Realizar Movimentações</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" media="screen"
	href="${css}main.css" />
</head>
<body>
	<c:import url="../../templates/navbar.jsp" />
	<main>
	<div>
		<h2 class="fw-l alg-text-centro">Adicione um novo usuário</h2>
		<div class="d-flex m-t-25">
			<form:form modelAttribute="movimentacao"
				action="${salvarmovimentacao}" method="post" id="alterar-senha"
				class="m-w-20">

				<div>
					<div class="m-t-respiro-m">
						<label>Item a ser movido</label>
						<div class="m-t-respiro-m">
							<form:select path="itemPatrimonio.id">

								<form:option value="${item.id}" />
							</form:select>
						</div>
					</div>
					<div class="m-t-respiro-m">
						<label>Ambiente atual</label>
						<div class="m-t-respiro-m">
							<form:select path="ambiente_origem.nome">
								<form:option value="${item.ambiente.nome}" />
							</form:select>
						</div>
					</div>
					<div class="m-t-respiro-m">
						<label>Novo ambiente</label>
						<div class="m-t-respiro-m">
							<form:select path="ambiente_destino.nome">
								<%--<form:options items="${ambientes}" itemLabel="nome"
									itemValue="id" />--%>
								<c:forEach items="${ambientes}" var="ambiente">
									<c:choose>
										<c:when test="${item.ambiente.nome eq ambiente.nome }">
											<form:option value="${ambiente.nome}" disabled="true" />
										</c:when>
										<c:otherwise>
											<form:option value="${ambiente.nome}" />
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</form:select>
						</div>
					</div>
					<button type="submit"
						class="cool-btn-action alg-text-centro m-t-respiro-g">Salvar</button>
				</div>
			</form:form>
		</div>
	</div>
	</main>
</body>
</html>