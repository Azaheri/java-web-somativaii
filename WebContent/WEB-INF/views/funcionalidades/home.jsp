<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- URLs --%>
<c:url value="/app/usuario" var="usuariod"/>
<c:url value="/app/ambiente" var="ambiente"/>
<c:url value="/app/patrimonio" var="patrimonio"/>
<c:url value="/app/categoria" var="categoria"/>
<c:url value="/app/item" var="itempatrimonio"/>
<c:url value="/app/movimentacao" var="movimentacao"/>
<c:url value="/sair" var="logout"/>
<c:url value="/assets/css/" var="css"/>
<%-- URLs --%>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SPKeeper - Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="${css}main.css" />
</head>
	<body>
	<c:import url="../../templates/navbar.jsp"/>
		<div class="d-block alg-text-centro m-t-40">
			<h4>Seja bem-vindo ${usuarioAutenticado.nome} ${usuarioAutenticado.sobrenome}</h4>
		</div>
	</body>
</html>