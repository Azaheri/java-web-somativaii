<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%-- URLs --%>
<c:url value="/app/adm/usuario/salvar" var="salvarUsuario" />
<c:url value="/app/adm/usuario/deletar" var="deletarUsuario" />
<c:url value="/assets/css/" var="css" />
<%-- URLs --%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>SPKeeper - Adicionar Usuários</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" media="screen"
	href="${css}main.css" />
</head>
<body>
	<c:import url="../../templates/navbar.jsp" />
	<main>
	<div>
		<c:if test="${empty usuario.id}">
			<h2 class="fw-l alg-text-centro">Adicione um usuário</h2>
		</c:if>
		<c:if test="${not empty usuario.id}">
			<h2 class="fw-l alg-text-centro">Alterando o usuário
				'${usuario.nome}'</h2>
		</c:if>
		
		<div class="d-flex m-t-25">
			<form:form modelAttribute="usuario" action="${salvarUsuario}"
				method="post" id="alterar-senha">
				<form:hidden path="id" />
				<div>
					<div class="m-t-respiro-p">
						<label>Nome</label>
						<form:input path="nome" required="required" />
						<form:errors path="nome" class="erro" />
					</div>
					<div class="m-t-respiro-p">
						<label>Sobrenome</label>
						<form:input path="sobrenome" required="required" />
						<form:errors path="sobrenome" class="erro" />
					</div>
					<c:if test="${empty usuario.id}">
						<div class="m-t-respiro-p">
							<label>E-mail</label>
							<form:input path="email" required="required" />
							<form:errors path="email" class="erro" />
						</div>
						<div class="m-t-respiro-p">
							<label>Senha</label>
							<form:password path="senha" required="required" />
							<form:errors path="senha" class="erro" />
						</div>
						<div class="m-t-respiro-p">
							<label>Confirmar senha</label> <input type="password"
								required="required" name="confirmacaoSenha" />
						</div>
					</c:if>
					<div class="m-t-respiro-m">
						<c:if test="${empty usuario.id}">
							<label>Administrador</label>
							<form:checkbox path="administrador" disabled="false" />
						</c:if>
						<c:if test="${not empty usuario.id}">
							<label>Administrador</label>
							<form:checkbox path="administrador" disabled="true" />
						</c:if>
					</div>
				</div>
				<button type="submit"
					class="cool-btn-action alg-text-centro m-t-respiro-g">Salvar</button>
				<c:if
					test="${not empty usuario.id and usuario.tipo != 'ADMINISTRADOR'}">
					<a href="${deletarUsuario}?id=${usuario.id}"
						class="cool-btn-action alg-text-centro m-t-respiro-g txt-d-none">Deletar</a>
				</c:if>
			</form:form>
		</div>
	</div>
	</main>
</body>
</html>