<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:url value="/app/adm/usuario/novo" var="addUsuario"/>
<c:url value="/app/adm/usuario" var="editUsuario"/>
<c:url value="/app/usuario/alterarSenha" var="editSenhaUsuario"/>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
    <body>
        <main>
            <ul>
                <li><a href="${editSenhaUsuario}">Alterar senha</a></li>
                <li><a href="${addUsuario}">Adicionar usuários</a></li>
                <li><a href="${editUsuario}">Listar usuários</a></li>
            </ul>
        </main>
    </body>
</html>