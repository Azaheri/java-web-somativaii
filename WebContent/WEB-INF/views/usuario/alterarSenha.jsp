<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%-- URLs --%>
<c:url value="/app/usuario/senha" var="salvarUsuario" />
<c:url value="/assets/css/" var="css" />
<%-- URLs --%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>SPKeeper - Alterar Senha</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" media="screen"
	href="${css}main.css" />
</head>
<body class="">
	<c:import url="../../templates/navbar.jsp" />

	<main>
	<div>
		<h2 class="fw-l alg-text-centro">Altere sua senha</h2>
		<div class="d-flex m-t-25">

			<form:form modelAttribute="usuario" action="${salvarUsuario}"
				method="post" id="alterar-senha">
				<form:hidden path="id" />
				<div>
					<div>
						<label>Senha antiga</label>
						<form:password path="senhaAntiga" required="required"
							class="alg-text-centro" placeholder="**********" />
						<form:errors path="senhaAntiga" class="erro" />
					</div>
					<div>
						<label>Nova senha</label>
						<form:password path="senha" required="required"
							class="alg-text-centro" placeholder="**********" />
						<form:errors path="senha" class="erro" />
					</div>
					<div>
						<label>Confirmação</label>
						<form:password path="confirmaSenha" required="required"
							class="alg-text-centro" placeholder="**********" />
						<form:errors path="confirmaSenha" class="erro" />
					</div>
					<button type="submit"
						class="cool-btn-action alg-text-centro m-t-respiro-g">Alterar
						senha</button>
				</div>
			</form:form>

		</div>
	</div>

	</main>
</body>
</html>