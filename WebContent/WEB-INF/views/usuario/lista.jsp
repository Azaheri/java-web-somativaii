<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%-- URLs --%>
<c:url value="/app/adm/usuario/editar" var="editarUsuario" />
<c:url value="/assets/css/" var="css" />
<%-- URLs --%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>SPKeeper - Lista de Usuários</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" media="screen"
	href="${css}main.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="${css}fontawesome-all.min.css" />
</head>
<body>
	<c:import url="../../templates/navbar.jsp" />
	<main>
	<h2 class="fw-l alg-text-centro">Usuários registrados</h2>
	<div>
		<table>
			<thead>
				<tr>
					<th>Id</th>
					<th>Nome completo</th>
					<th>E-mail</th>
					<th>Tipo</th>
					<th>Editar</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${usuarios}" var="usuario">
					<tr>
						<td>${usuario.id}</td>
						<td><c:out value="${usuario.nome} ${usuario.sobrenome}"
								escapeXml="true" /></td>
						<td>${usuario.email}</td>
						<td>${usuario.tipo}</td>
						<td><a href="${editarUsuario}?id=${usuario.id}"> <i
								class="fas fa-user-edit fa-2x editar"></i>
						</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	</main>
</body>
</html>