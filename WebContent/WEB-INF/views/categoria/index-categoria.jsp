<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- URLs --%>
<c:url value="/app/categoria/lista" var="listaCategoria"/>
<c:url value="/app/adm/categoria/nova" var="addCategoria"/>
<c:url value="/app/adm/categoria/editar" var="editCategoria"/>
<c:url value="/app/adm/categoria/deletar" var="deletarCategoria"/>
<c:url value="/app" var="home"/>
<%-- URLs --%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
    <body>
        <main>
            <div>
            	<ul>
            		<li><a href="${listaCategoria}">Lista de categorias</a></li>
            		<li><a href="${addCategoria}">Novo categoria</a></li>
            	</ul>
            </div>
        </main>
    </body>
</html>