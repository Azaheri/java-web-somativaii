<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- URLs --%>
<c:url value="/app/categoria/lista" var="listaCategoria"/>
<c:url value="/app/adm/categoria/editar" var="editCategoria"/>
<c:url value="/app/adm/categoria/nova" var="addCategoria"/>
<c:url value="/app/adm/categoria/deletar" var="deletarCategoria"/>
<c:url value="/app" var="home"/>
<c:url value="/assets/css/" var="css"/>
<%-- URLs --%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SPKeeper - Listar Categorias</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="${css}main.css" />
</head>
    <body>
    <c:import url="../../templates/navbar.jsp"/>
        <main>
        <h2 class="fw-l alg-text-centro">Categorias cadastradas</h2>
            <div>
            	<div>
            		<table>
            			<thead>
            				<tr>
            					<th>Id</th>
            					<th>Categorias</th>
            					<c:if test="${usuarioAutenticado.tipo == 'ADMINISTRADOR'}">
            						<th>Editar</th>
            					</c:if>
            				</tr>
            			</thead>
            			<tbody>
            				<c:forEach items="${categorias}" var="categoria">
            					<tr>
            						<td>
            							${categoria.id}
            						</td>
            						<td>
            							<c:out value="${categoria.nome}" escapeXml="true"/>
            						</td>
            						<c:if test="${usuarioAutenticado.tipo == 'ADMINISTRADOR'}">
	            						<td>
	            							<a href="${editCategoria}?id=${categoria.id}">
	            								<i class="fas fa-edit fa-2x editar"></i>
	            							</a>
	            						</td>
	            					</c:if>
            					</tr>
            				</c:forEach>
            			</tbody>
            		</table>
            	</div>
            </div>
        </main>
    </body>
</html>