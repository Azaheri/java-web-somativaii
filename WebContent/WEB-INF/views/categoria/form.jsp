<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%-- URLs --%>
<c:url value="/app/categoria/lista" var="listacategoria" />
<c:url value="/app/adm/categoria/editar" var="editcategoria" />
<c:url value="/app/adm/categoria/nova" var="addcategoria" />
<c:url value="/app/adm/categoria/deletar" var="deletarcategoria" />
<c:url value="/app/adm/categoria/salvar" var="salvarcategoria" />
<c:url value="/app" var="home" />
<c:url value="/assets/css/" var="css" />
<%-- URLs --%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>SPKeeper - Adicionar Categorias</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" media="screen"
	href="${css}main.css" />
</head>
<body>
	<c:import url="../../templates/navbar.jsp" />
	<main>
	<div>
		<c:if test="${empty categoria.id}">
			<h2 class="fw-l alg-text-centro">Adicione uma nova categoria</h2>
		</c:if>
		<c:if test="${not empty categoria.id}">
			<h2 class="fw-l alg-text-centro">Alterando a categoria
				'${categoria.nome}'</h2>
		</c:if>
		
		<div class="d-flex m-t-25">
			<form:form modelAttribute="categoria" action="${salvarcategoria}"
				method="post" id="alterar-senha">
				<form:hidden path="id" />
				<div>
					<div>
						<label>Nome</label>
						<form:input path="nome" autofocus="autofocus" required="required" />
						<form:errors path="nome" class="erro" />
					</div>
				</div>
				<button type="submit"
					class="cool-btn-action alg-text-centro m-t-respiro-g">Salvar</button>
				<c:if test="${not empty categoria.id}">
					<a href="${deletarcategoria}?id=${categoria.id}"
						class="cool-btn-action alg-text-centro m-t-respiro-g txt-d-none">Deletar</a>
				</c:if>
			</form:form>
		</div>
	</div>
	</main>
</body>
</html>