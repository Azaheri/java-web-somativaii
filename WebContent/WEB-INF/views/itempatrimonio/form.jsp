<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%-- URLs --%>
<c:url value="/app/itempatrimonio/lista" var="listaitempatrimonio" />
<c:url value="/app/itempatrimonio/novo" var="additempatrimonio" />
<c:url value="/app/adm/itempatrimonio/deletar"
	var="deletaritempatrimonio" />
<c:url value="/app/itempatrimonio/salvar" var="salvaritempatrimonio" />
<c:url value="/app" var="home" />
<c:url value="/assets/css/" var="css" />
<%-- URLs --%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>SPKeeper - Adicionar Item</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" media="screen"
	href="${css}main.css" />
</head>
<body>
	<c:import url="../../templates/navbar.jsp" />
	<main>
	<div>
		<h2 class="fw-l alg-text-centro">Adicione um novo item de
			patrimônio</h2>
		<div class="d-flex m-t-25">
			<form:form modelAttribute="itempatrimonio"
				action="${salvaritempatrimonio}" method="post" id="alterar-senha">
				<form:hidden path="id" />
				<c:if test="${not empty patrimonios and not empty ambientes}">
				<div>
					<div class="m-t-respiro-p">
						<label>Patrimônio</label>
						<form:select path="patrimonio.id" items="${patrimonios}"
							itemLabel="nome" itemValue="id" />
					</div>
					<div class="m-t-respiro-p">
						<label>Ambiente</label>
						<form:select path="ambiente.id" items="${ambientes}"
							itemLabel="nome" itemValue="id" />
					</div>
				</div>
					<button type="submit"
					class="cool-btn-action alg-text-centro m-t-respiro-g">Salvar</button>
					</c:if>
					<c:if test="${empty patrimonios or empty ambientes }">
						<div style="min-width: 400px;">
							<h5 class="fw-l alg-text-centro">Favor registrar um patrimônio e um ambiente antes de cadastrar um item de patrimônio</h5>
						</div>
					</c:if>
				<c:if test="${not empty itempatrimonio.id}">
					<a href="${deletaritempatrimonio}?id=${itempatrimonio.id}"
						class="cool-btn-action alg-text-centro m-t-respiro-g txt-d-none">Deletar</a>
				</c:if>
			</form:form>
		</div>
	</div>
	</main>
</body>
</html>