<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%-- URLs --%>
<c:url value="/app/itempatrimonio/lista" var="listaitempatrimonio"/>
<c:url value="/app/itempatrimonio/novo" var="additempatrimonio"/>
<c:url value="/app/itempatrimonio/detalhe" var="detalhe"/>
<c:url value="/app/adm/itempatrimonio/deletar" var="deletaritempatrimonio"/>
<c:url value="/app" var="home"/>
<c:url value="/assets/css/" var="css"/>
<%-- URLs --%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SPKeeper - Listar Itens</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="${css}main.css" />
</head>
    <body>
    <c:import url="../../templates/navbar.jsp"/>
        <main>
        <h2 class="fw-l alg-text-centro">Itens de patrimônios cadastrados</h2>
            <div>
            	<div>
            		<table>
            			<thead>
            				<tr>
            					<th>Id</th>
            					<th>Patrimônio</th>
            					<th>Detalhes</th>
            				</tr>
            			</thead>
            			<tbody>
            				<c:forEach items="${itempatrimonios}" var="itempatrimonio">
            					<tr>
            						<td>
            							${itempatrimonio.id}
            						</td>
            						<td>
            							<c:out value="${itempatrimonio.patrimonio.nome}" escapeXml="true"/>
            						</td>
            						<td>
            							<a href="${detalhe}?id=${itempatrimonio.id}">
            								<i class="fas fa-clipboard-list fa-2x editar"></i>
            							</a>
            						</td>
            					</tr>
            				</c:forEach>
            			</tbody>
            		</table>
            	</div>
            </div>
        </main>
    </body>
</html>