<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%-- URLs --%>
<c:url value="/app/adm/itempatrimonio/deletar" var="deletaritem"/>
<c:url value="/app/itempatrimonio/movimentar" var="movimentarItem"/>
<c:url value="/assets/css/" var="css"/>
<%-- URLs --%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SPKeeper - Item Detalhado</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" media="screen" href="${css}main.css" />
</head>
	<body>
	<c:import url="../../templates/navbar.jsp"/>
		<h2 class="fw-l alg-text-centro">Detalhes do item ${itemdetalhe.id}</h2>
		<div class="d-flex just-cont-centro">
		<form:form modelAttribute="itemdetalhe">
			<form:hidden path="ambiente.id" var="ambiente.id"/>
			<form:hidden path="id" var="id"/>
		</form:form>
			<div class="m-t-25">
			<p>Tipo de patrimonio: <c:out value="${itemdetalhe.patrimonio.nome}" escapeXml="true"/></p>
			<p>Categoria do patrimônio: <c:out value="${itemdetalhe.patrimonio.categoria.nome}" escapeXml="true"/></p>
			<p>Local atual do item: <c:out value="${itemdetalhe.ambiente.nome}" escapeXml="true"/></p>
			<p>Nome de quem cadastrou: <c:out value="${itemdetalhe.usuario.nome} ${itemdetalhe.usuario.sobrenome}" escapeXml="true"/></p>
				<c:if test="${itemdetalhe.ultimaMovimentacao != null}">
					<p>Data da última movimentação:<fmt:formatDate value="${itemdetalhe.ultimaMovimentacao}" pattern="dd/MM/yyyy kk:mm:ss"/> </p>
				</c:if>
				<c:if test="${itemdetalhe.ultimaMovimentacao eq null}">
					<p>Data da última movimentação: Este item ainda não foi movimentado</p>
				</c:if>
			
			<c:if test="${not empty itemdetalhe.id}">
				<div class="d-flex just-cont-centro">
					<c:if test="${usuarioAutenticado.tipo == 'ADMINISTRADOR'}">
						<a href="${deletaritem}?id=${itemdetalhe.id}" class="cool-btn-action alg-text-centro m-t-respiro-g txt-d-none alg-text-centro">Deletar</a>
					</c:if>
					<a href="${movimentarItem}?id=${itemdetalhe.id}" class="cool-btn-action alg-text-centro m-t-respiro-g txt-d-none alg-text-centro">Movimentar</a>
				</div>
			</c:if>
			</div>
		</div>
	</body>
</html>