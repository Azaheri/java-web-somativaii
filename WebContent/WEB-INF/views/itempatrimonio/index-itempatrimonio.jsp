<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- URLs --%>
<c:url value="/app/itempatrimonio/lista" var="listaitempatrimonio"/>
<c:url value="/app/itempatrimonio/novo" var="additempatrimonio"/>
<c:url value="/app/adm/itempatrimonio/deletar" var="deletaritempatrimonio"/>
<c:url value="/app" var="home"/>
<%-- URLs --%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
    <body>
        <main>
            <div>
            	<ul>
            		<li><a href="${listaitempatrimonio}">Lista de itens do patrimônio</a></li>
            		<li><a href="${additempatrimonio}">Novo Item de patrimônio</a></li>
            	</ul>
            </div>
        </main>
    </body>
</html>