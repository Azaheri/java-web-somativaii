<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:url value="/" var="raiz"/>
<c:url value="/usuario/autenticar" var="urlAutenticarUsuario"/>
<c:url value="/assets/css/" var="css"/>
<c:url value="/assets/imagens/" var="img"/>
<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8" />
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <title>PKeeper - HOME</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" media="screen" href="${css}main.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="${css}fontawesome-all.min.css" />
		<link rel="icon" href="${img}logo-senaipkeeper.svg" type="image/x-icon" />
	</head>
	<body class="d-flex alg-item-centro bg-index">
			<div class="d-flex alg-item-centro flex-1 flex-dir-col" id="ic-ip-index">
				<div class="vh-65">
					<img alt="Logo Senai Patrimony Keeper" src="${img}logo-senaipkeeper.svg" class="img-maior">
				</div>
				<div>
					<form:form modelAttribute="usuario" action="${urlAutenticarUsuario}" method="post" class="d-flex alg-item-centro flex-dir-col">
						<label class="flex-dir-col d-flex alg-item-centro p-relative vh-7">
							<form:input path="email" type="email" class="max-w-g font-ip-index cool-input" placeholder="exemplo@email.com" required="required" maxlength="100" id="inputEmail" autofocus="autofocus"/>
							<i class="fas fa-envelope ic-pos p-absolute"></i>
							<form:errors path="email" class="erro"/>
						</label>
						<label class="flex-dir-col d-flex alg-item-centro p-relative vh-7">
							<form:password path="senha" class="max-w-g font-ip-index cool-input" placeholder="******" required="required" maxlength="20" />
							<i class="fas fa-key ic-pos p-absolute"></i>
							<form:errors path="senha" class="erro"/>
						</label>
						<div class="p-relative">
							<button type="submit" class="cool-btn-login">Fazer Login</button>
							<i class="fas fa-sign-in-alt ic-btn p-absolute"></i>
						</div>
					</form:form>
				</div>
			</div>
	</body>
</html>