<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- URLs --%>
<c:url value="/app/adm/patrimonio/editar" var="editpatrimonio"/>
<c:url value="/app/patrimonio/lista" var="listapatrimonio"/>
<c:url value="/app/adm/patrimonio/novo" var="addpatrimonio"/>
<c:url value="/app/adm/patrimonio/deletar" var="deletarpatrimonio"/>
<c:url value="/app" var="home"/>
<%-- URLs --%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
    <body>
        <main>
            <div>
            	<ul>
            		<li><a href="${listapatrimonio}">Lista de patrimonios</a></li>
            		<li><a href="${addpatrimonio}">Novo patrimonio</a></li>
            	</ul>
            </div>
        </main>
    </body>
</html>