<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%-- URLs --%>
<c:url value="/app/patrimonio/lista" var="listapatrimonio" />
<c:url value="/app/adm/patrimonio/editar" var="editpatrimonio" />
<c:url value="/app/adm/patrimonio/novo" var="addpatrimonio" />
<c:url value="/app/adm/patrimonio/deletar" var="deletarpatrimonio" />
<c:url value="/app/adm/patrimonio/salvar" var="salvarpatrimonio" />
<c:url value="/app" var="home" />
<c:url value="/assets/css/" var="css" />
<%-- URLs --%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>SPKeeper - Adicionar Patrimônios</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" media="screen"
	href="${css}main.css" />
</head>
<body>
	<c:import url="../../templates/navbar.jsp" />
	<main>
	<div>
		<c:if test="${empty patrimonio.id}">
		<h2 class="fw-l alg-text-centro">Adicione um novo patrimônio</h2>
		</c:if>
		<c:if test="${not empty patrimonio.id}">
			<h2 class="fw-l alg-text-centro">Alterando o patrimônio
				'${patrimonio.nome}'</h2>
		</c:if>
		<div class="d-flex m-t-25">
			<form:form modelAttribute="patrimonio" action="${salvarpatrimonio}"
				method="post" id="alterar-senha">
				<form:hidden path="id" />
				<form:hidden path="categoria.nome"/>
				<c:if test="${not empty categorias}">
				<div>
					<div>
						<label>Nome</label>
						<form:input path="nome" autofocus="autofocus" required="required" />
						<form:errors path="nome" class="erro" />
					</div>
					<div class="m-t-respiro-g">
						<form:select path="categoria.id" items="${categorias}"
							itemLabel="nome" itemValue="id" />
					</div>
				</div>
					<button type="submit"
					class="cool-btn-action alg-text-centro m-t-respiro-g">Salvar</button>
				</c:if>
					<c:if test="${empty categorias}">
						<div style="min-width: 400px;">
							<h5 class="fw-l alg-text-centro">Favor registrar uma categoria antes de cadastrar um patrimônio</h5>
						</div>
					</c:if>
				<c:if test="${not empty patrimonio.id}">
					<a href="${deletarpatrimonio}?id=${patrimonio.id}"
						class="cool-btn-action alg-text-centro m-t-respiro-g txt-d-none">Deletar</a>
				</c:if>
			</form:form>
		</div>
	</div>
	</main>
</body>
</html>