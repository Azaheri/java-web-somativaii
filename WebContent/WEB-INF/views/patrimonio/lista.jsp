<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- URLs --%>
<c:url value="/app/patrimonio/lista" var="listapatrimonio"/>
<c:url value="/app/adm/patrimonio/editar" var="editpatrimonio"/>
<c:url value="/app/adm/patrimonio/novo" var="addpatrimonio"/>
<c:url value="/app/adm/patrimonio/deletar" var="deletarpatrimonio"/>
<c:url value="/app" var="home"/>
<c:url value="/assets/css/" var="css"/>
<%-- URLs --%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SPKeeper - Adicionar Patrimônios</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="${css}main.css" />
</head>
    <body>
    <c:import url="../../templates/navbar.jsp"/>
        <main>
        <h2 class="fw-l alg-text-centro">Patrimônios cadastrados</h2>
            <div>
            	<div>
            		<table>
            			<thead>
            				<tr>
            					<th>Id</th>
            					<th>Nome</th>
            					<th>Categoria</th>
            					<c:if test="${usuarioAutenticado.tipo == 'ADMINISTRADOR'}">
            						<th>Editar</th>
            					</c:if>
            				</tr>
            			</thead>
            			<tbody>
            				<c:forEach items="${patrimonios}" var="patrimonio">
            					<tr>
            						<td>
            							${patrimonio.id}
            						</td>
            						<td>
            							<c:out value="${patrimonio.nome}" escapeXml="true"/>
            						</td>
            						<td>
            							<c:out value="${patrimonio.categoria}" escapeXml="true"/>
            						</td>
            						<c:if test="${usuarioAutenticado.tipo == 'ADMINISTRADOR'}">
            						<td>
            							<a href="${editpatrimonio}?id=${patrimonio.id}">
            								<i class="fas fa-edit fa-2x editar"></i>
            							</a>
            						</td>
            						</c:if>
            					</tr>
            				</c:forEach>
            			</tbody>
            		</table>
            	</div>
            </div>
        </main>
    </body>
</html>