<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%-- URLs --%>
<c:url value="/app/ambiente/lista" var="listaAmbiente" />
<c:url value="/app/adm/ambiente/editar" var="editAmbiente" />
<c:url value="/app/adm/ambiente/nova" var="addAmbiente" />
<c:url value="/app/adm/ambiente/deletar" var="deletarAmbiente" />
<c:url value="/app/adm/ambiente/salvar" var="salvarAmbiente" />
<c:url value="/app" var="home" />
<c:url value="/assets/css/" var="css" />
<%-- URLs --%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>SPKeeper - Adicionar Ambientes</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" media="screen"
	href="${css}main.css" />
</head>
<body>
	<c:import url="../../templates/navbar.jsp" />
	<main>
	<div>
		<c:if test="${empty ambiente.id}">
			<h2 class="fw-l alg-text-centro">Adicione um novo ambiente</h2>
		</c:if>
		<c:if test="${not empty ambiente.id}">
			<h2 class="fw-l alg-text-centro">Alterando o ambiente '${ambiente.nome}'</h2>
		</c:if>
		<div class="d-flex m-t-25">
			<form:form modelAttribute="ambiente" action="${salvarAmbiente}"
				method="post" id="alterar-senha">
				<form:hidden path="id" />
				<div>
					<div>
						<label>Nome</label>
						<form:input path="nome" autofocus="autofocus" required="required" />
						<form:errors path="nome" class="erro" />
					</div>
				</div>
				<button type="submit"
					class="cool-btn-action alg-text-centro m-t-respiro-g">Salvar</button>
				<c:if test="${not empty ambiente.id}">
					<a href="${deletarAmbiente}?id=${ambiente.id}"
						class="cool-btn-action alg-text-centro m-t-respiro-g txt-d-none">Deletar</a>
				</c:if>
			</form:form>
		</div>
	</div>
	</main>
</body>
</html>