<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- URLs --%>
<c:url value="/app/ambiente/lista" var="listaAmbiente"/>
<c:url value="/app/adm/ambiente/nova" var="addAmbiente"/>
<c:url value="/app/adm/ambiente/editar" var="editAmbiente"/>
<c:url value="/app/adm/ambiente/deletar" var="deletarAmbiente"/>
<c:url value="/app" var="home"/>
<%-- URLs --%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
    <body>
        <main>
            <div>
            	<ul>
            		<li><a href="${listaAmbiente}">Lista de ambientes</a></li>
            		<li><a href="${addAmbiente}">Novo ambiente</a></li>
            	</ul>
            </div>
        </main>
    </body>
</html>