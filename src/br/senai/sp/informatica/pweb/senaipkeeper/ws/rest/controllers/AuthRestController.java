package br.senai.sp.informatica.pweb.senaipkeeper.ws.rest.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.senai.sp.informatica.pweb.senaipkeeper.exceptions.EndPointException;
import br.senai.sp.informatica.pweb.senaipkeeper.exceptions.EntidadeNaoEncontrada;
import br.senai.sp.informatica.pweb.senaipkeeper.exceptions.ValidacaoException;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Usuario;
import br.senai.sp.informatica.pweb.senaipkeeper.services.UsuarioService;
import br.senai.sp.informatica.pweb.senaipkeeper.utils.JwtUtils;
import br.senai.sp.informatica.pweb.senaipkeeper.utils.MapUtils;

@RestController
@RequestMapping("/rest/auth")
public class AuthRestController 
{
	@Autowired
	private UsuarioService usuarioService;
	
	@PostMapping(value = {"/jwt", "/jwt/", "/**", "/jwt**"})
	public ResponseEntity<Object> gerarJWT(@Valid @RequestBody Usuario usuario, BindingResult bindingResult)
	{
		try 
		{
			Usuario usuarioBuscado = usuarioService.buscarEmailESenha(usuario, bindingResult);
			Map<String, String> mapaToken = new HashMap<>();
			mapaToken.put("token", JwtUtils.gerarToken(usuarioBuscado));
			return ResponseEntity.ok(mapaToken);
		}
		catch (ValidacaoException e) 
		{
			return ResponseEntity.status(422).body(MapUtils.mapaDe(bindingResult));
		}
		catch (EntidadeNaoEncontrada e) 
		{
			return ResponseEntity.notFound().header("X-Reason", "Entidade n�o encontrada").build();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}
}