package br.senai.sp.informatica.pweb.senaipkeeper.ws.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.senai.sp.informatica.pweb.senaipkeeper.services.AmbienteService;

@RestController
@RequestMapping("/rest/ambientes")
public class AmbienteRestController
{
	@Autowired
	private AmbienteService ambienteService;
	
	@GetMapping("/**")
	public ResponseEntity<Object> buscarTodos()
	{
		try
		{
			return ResponseEntity.ok(ambienteService.buscarTodos());
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return ResponseEntity.status(500).build();
		}
	}
	
}
