package br.senai.sp.informatica.pweb.senaipkeeper.ws.rest.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.senai.sp.informatica.pweb.senaipkeeper.exceptions.EndPointException;
import br.senai.sp.informatica.pweb.senaipkeeper.exceptions.EntidadeNaoEncontrada;
import br.senai.sp.informatica.pweb.senaipkeeper.exceptions.ValidacaoException;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Movimentacao;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Usuario;
import br.senai.sp.informatica.pweb.senaipkeeper.models.UsuarioBase;
import br.senai.sp.informatica.pweb.senaipkeeper.models.UsuarioJWT;
import br.senai.sp.informatica.pweb.senaipkeeper.services.ItemPatrimonioService;
import br.senai.sp.informatica.pweb.senaipkeeper.utils.MapUtils;

@RestController
@RequestMapping("/rest/itens")
public class ItemRestController 
{
	@Autowired
	private ItemPatrimonioService itemPatrimonioService;
	
	@GetMapping("/{id}")
	public ResponseEntity<Object> buscar(@PathVariable Long id)
	{
		try 
		{
			return ResponseEntity.ok(itemPatrimonioService.buscar(id));
		} 
		catch (EntidadeNaoEncontrada e) 
		{
			return ResponseEntity.notFound().build();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return ResponseEntity.status(500).build();
		}
	}
	
	@GetMapping
	public ResponseEntity<Object> buscarTodos()
	{
		try
		{
			return ResponseEntity.ok(itemPatrimonioService.buscarTodos());
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return ResponseEntity.status(500).build();
		}
	}
	
	@GetMapping("/{id}/movimentacoes")
	public ResponseEntity<Object> buscarMovimentacoes(@PathVariable Long id)
	{
		try 
		{
			return ResponseEntity.ok(itemPatrimonioService.movimentacoes(id));
		} 
		catch (EntidadeNaoEncontrada e) {
			return ResponseEntity.notFound().build();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return ResponseEntity.status(500).build();
		}
	}
	
	@PostMapping("/{id}/movimentacoes")
	public ResponseEntity<Object> moverItem(@PathVariable Long id, @RequestBody @Valid Movimentacao movimentacao, BindingResult bindingResult, UsuarioJWT usuario)
	{
		try 
		{
					
			return ResponseEntity.ok(itemPatrimonioService.movimentarItem(id, movimentacao, bindingResult));
		}
		catch (ValidacaoException e) 
		{
			return ResponseEntity.unprocessableEntity().body(MapUtils.mapaDe(bindingResult));
		}
		catch (EntidadeNaoEncontrada e) 
		{
			return ResponseEntity.notFound().build();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return ResponseEntity.status(500).build();
		}
	}
}