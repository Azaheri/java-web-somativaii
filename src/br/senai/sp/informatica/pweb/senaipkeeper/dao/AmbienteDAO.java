package br.senai.sp.informatica.pweb.senaipkeeper.dao;

import java.util.List;

import br.senai.sp.informatica.pweb.senaipkeeper.models.Ambiente;

public interface AmbienteDAO {

	public void alterar(Ambiente obj);
	
	public Ambiente buscar(Long id);
	
	public Ambiente buscarPorNome(String nome);
	
	public List<Ambiente> buscarTodos();
	
	public void deletar(Ambiente obj);
	
	public void persistir(Ambiente obj);	
}