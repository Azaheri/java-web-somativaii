package br.senai.sp.informatica.pweb.senaipkeeper.dao;

import java.util.List;

import br.senai.sp.informatica.pweb.senaipkeeper.models.Usuario;

public interface UsuarioDAO {

	public void alterar(Usuario obj);
	
	public Usuario buscar(Long id);
	
	public List<Usuario> buscarTodos();
	
	public void deletar(Usuario obj);
	
	public void persistir(Usuario obj);
	
//	public void alterarSenha(Usuario obj);
	
	public Usuario buscarPorEmail(String email);
	
	public Usuario buscarPorEmailESenha(String email, String senha);
	
}