package br.senai.sp.informatica.pweb.senaipkeeper.dao;

import java.util.List;

import br.senai.sp.informatica.pweb.senaipkeeper.models.ItemPatrimonio;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Movimentacao;

public interface MovimentacaoDAO {

	public Movimentacao buscar(Long id);
	
	public List<Movimentacao> buscarTodos();
	
	public void persistir(Movimentacao obj);
	
	public Movimentacao movimentacoesDeItens(ItemPatrimonio itemPatrimonio);
}