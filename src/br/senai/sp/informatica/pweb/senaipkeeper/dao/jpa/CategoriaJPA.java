package br.senai.sp.informatica.pweb.senaipkeeper.dao.jpa;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.senai.sp.informatica.pweb.senaipkeeper.dao.CategoriaDAO;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Ambiente;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Categoria;

@Transactional
@Repository
public class CategoriaJPA implements CategoriaDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void alterar(Categoria obj) {
		sessionFactory.getCurrentSession().update(obj);
	}

	@Override
	public Categoria buscar(Long id) {
		String hql = "FROM Categoria c WHERE c.id = :id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("id", id);
		List<Categoria> resultados = query.list();
		if (!resultados.isEmpty()) {
			return resultados.get(0);
		}
		return null;
	}

	@Override
	public List<Categoria> buscarTodas() {
		String hql = "FROM Categoria c  ORDER BY c.id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}

	@Override
	public void deletar(Categoria obj) {
		sessionFactory.getCurrentSession().delete(obj);
	}

	@Override
	public void persistir(Categoria obj) {
		sessionFactory.getCurrentSession().persist(obj);
	}

	@Override
	public Categoria buscarPorNome(String nome) {
		String hql = "FROM Categoria c WHERE c.nome = :nome";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("nome", nome);
		List<Categoria> resultados = query.list();
		
		if (!resultados.isEmpty()) {
			return resultados.get(0);
		}
		return null;
	}
}