package br.senai.sp.informatica.pweb.senaipkeeper.dao.jpa;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.senai.sp.informatica.pweb.senaipkeeper.dao.MovimentacaoDAO;
import br.senai.sp.informatica.pweb.senaipkeeper.models.ItemPatrimonio;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Movimentacao;

@Transactional
@Repository
public class MovimentacaoJPA implements MovimentacaoDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Movimentacao buscar(Long id) {
		String hql = "FROM Movimentacao m WHERE m.id = :id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("id", id);
		List<Movimentacao> resultados = query.list();
		if (!resultados.isEmpty()) {
			return resultados.get(0);
		}
		return null;
	}

	@Override
	public List<Movimentacao> buscarTodos() {
		String hql = "FROM Movimentacao m  ORDER BY m.id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}

	@Override
	public void persistir(Movimentacao obj) {
		sessionFactory.getCurrentSession().persist(obj);
	}

	@Override
	public Movimentacao movimentacoesDeItens(ItemPatrimonio idItem){
		String hql = "FROM Movimentacao m WHERE m.itemPatrimonio.id = :id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("id", idItem.getId());
		List<Movimentacao> resultados = query.list();
		if (!resultados.isEmpty()) {
			return resultados.get(resultados.size() - 1);
		}
		return null;
	}
	
}
