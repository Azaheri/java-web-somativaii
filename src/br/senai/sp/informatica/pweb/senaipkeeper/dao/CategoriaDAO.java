package br.senai.sp.informatica.pweb.senaipkeeper.dao;

import java.util.List;

import br.senai.sp.informatica.pweb.senaipkeeper.models.Categoria;

public interface CategoriaDAO {

	public void alterar(Categoria obj);
	
	public Categoria buscar(Long id);
	
	public List<Categoria> buscarTodas();
	
	public Categoria buscarPorNome(String nome);
	
	public void deletar(Categoria obj);
	
	public void persistir(Categoria obj);	
}