package br.senai.sp.informatica.pweb.senaipkeeper.dao;

import java.util.List;

import br.senai.sp.informatica.pweb.senaipkeeper.models.ItemPatrimonio;

public interface ItemPatrimonioDAO {

	public void alterar(ItemPatrimonio obj);
	
	public ItemPatrimonio buscar(Long id);
	
	public List<ItemPatrimonio> buscarTodos();
	
//	public ItemPatrimonio buscarPorNome(Long id);
	
	public void deletar(ItemPatrimonio obj);
	
	public void persistir(ItemPatrimonio obj);	
}