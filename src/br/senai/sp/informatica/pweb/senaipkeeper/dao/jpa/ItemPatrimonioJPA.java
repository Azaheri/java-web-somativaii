package br.senai.sp.informatica.pweb.senaipkeeper.dao.jpa;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.senai.sp.informatica.pweb.senaipkeeper.dao.ItemPatrimonioDAO;
import br.senai.sp.informatica.pweb.senaipkeeper.models.ItemPatrimonio;

@Transactional
@Repository
public class ItemPatrimonioJPA implements ItemPatrimonioDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	//N�o pedido
	@Override
	public void alterar(ItemPatrimonio obj) {
		sessionFactory.getCurrentSession().update(obj);
	}

	@Override
	public ItemPatrimonio buscar(Long id) {
		String hql = "FROM ItemPatrimonio i WHERE i.id = :id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("id", id);
		List<ItemPatrimonio> resultados = query.list();
		if (!resultados.isEmpty()) {
			return resultados.get(0);
		}
		return null;
	}

	@Override
	public List<ItemPatrimonio> buscarTodos() {
		String hql = "FROM ItemPatrimonio i  ORDER BY i.id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}

	//N�o pedido
//	@Override
//	public ItemPatrimonio buscarPorNome(Long id) {
//		String hql = "FROM ItemPatrimonio AS i INNER JOIN Patrimonio.nome AS p WHERE p.nome = :id";
//		Query query = sessionFactory.getCurrentSession().createQuery(hql);
//		query.setParameter("id", id);
//		List<ItemPatrimonio> resultados = query.list();
//		
//		if (!resultados.isEmpty()) {
//			return resultados.get(0);
//		}
//		return null;
//	}

	@Override
	public void deletar(ItemPatrimonio obj) {
		sessionFactory.getCurrentSession().delete(obj);
	}

	@Override
	public void persistir(ItemPatrimonio obj) {
		sessionFactory.getCurrentSession().persist(obj);
	}
}