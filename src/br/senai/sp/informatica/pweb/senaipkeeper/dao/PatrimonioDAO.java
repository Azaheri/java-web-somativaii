package br.senai.sp.informatica.pweb.senaipkeeper.dao;

import java.util.List;

import br.senai.sp.informatica.pweb.senaipkeeper.models.Patrimonio;

public interface PatrimonioDAO {

	public void alterar(Patrimonio obj);
	
	public Patrimonio buscar(Long id);
	
	public List<Patrimonio> buscarTodos();
	
	public Patrimonio buscarPorNome(String nome);
	
	public void deletar(Patrimonio obj);
	
	public void persistir(Patrimonio obj);	
}