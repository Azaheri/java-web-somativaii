package br.senai.sp.informatica.pweb.senaipkeeper.jobs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import br.senai.sp.informatica.pweb.senaipkeeper.dao.UsuarioDAO;
import br.senai.sp.informatica.pweb.senaipkeeper.models.TiposUsuarios;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Usuario;

@Component
public class CriarAdministradorJob implements ApplicationListener<ContextRefreshedEvent>{

	@Autowired
	private UsuarioDAO usuarioDAO;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		//Usu�rio administrador principal
		System.out.println("[JOB]Cria��o de usu�rio administrador padr�o");
		Usuario admin = new Usuario();
		admin.setEmail("admin@email.com");
		admin.setNome("Administrador");
		admin.setSobrenome("Adm");
		admin.setTipo(TiposUsuarios.ADMINISTRADOR);
		admin.setSenha("admin123");
		admin.senhaHash();
		
		System.out.println("[JOB]Verificando se o usu�rio administrador existe");
		if (usuarioDAO.buscarPorEmail(admin.getEmail()) == null) {
			System.out.println("[JOB]Criando o usu�rio administrador");
			usuarioDAO.persistir(admin);
		}
		System.out.println("[JOB]Usu�rio administrador j� existe");
		System.out.println("[JOB]Usu�rio administrador pronto para uso");
		
		
		//Usu�rio administrador secund�rio(teste)
		System.out.println("[JOB]Cria��o de usu�rio administrador teste");
		Usuario adminTeste = new Usuario();
		adminTeste.setEmail("adminteste@email.com");
		adminTeste.setNome("Administrador");
		adminTeste.setSobrenome("TESTE");
		adminTeste.setTipo(TiposUsuarios.ADMINISTRADOR);
		adminTeste.setSenha("admin321");
		adminTeste.senhaHash();
		
		System.out.println("[JOB]Verificando se o usu�rio administradorteste existe");
		if (usuarioDAO.buscarPorEmail(adminTeste.getEmail()) == null) {
			System.out.println("[JOB]Criando o usu�rio administradorteste");
			usuarioDAO.persistir(adminTeste);
		}
		System.out.println("[JOB]Usu�rio administradorteste j� existe");
		System.out.println("[JOB]Usu�rio administradorteste pronto para uso");
		
		
		//Usu�rio comum
		System.out.println("[JOB]Cria��o de usu�rio comum teste");
		Usuario comum = new Usuario();
		comum.setEmail("comum@email.com");
		comum.setNome("comum");
		comum.setSobrenome("teste");
		comum.setTipo(TiposUsuarios.COMUM);
		comum.setSenha("comum123");
		comum.senhaHash();
		
		System.out.println("[JOB]Verificando se o usu�rio comum existe");
		if (usuarioDAO.buscarPorEmail(comum.getEmail()) == null) {
			System.out.println("[JOB]Criando o usu�rio comum");
			usuarioDAO.persistir(comum);
		}
		System.out.println("[JOB]Usu�rio comum j� existe");
		System.out.println("[JOB]Usu�rio comum pronto para uso");
	}
}