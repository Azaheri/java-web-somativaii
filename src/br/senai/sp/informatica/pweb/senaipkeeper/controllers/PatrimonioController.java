package br.senai.sp.informatica.pweb.senaipkeeper.controllers;

import java.util.Date;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.senai.sp.informatica.pweb.senaipkeeper.dao.CategoriaDAO;
import br.senai.sp.informatica.pweb.senaipkeeper.dao.PatrimonioDAO;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Patrimonio;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Usuario;

@Controller
@RequestMapping("/app")
public class PatrimonioController {
	
	@Autowired
	private PatrimonioDAO patrimonioDAO;
	
	@Autowired
	private CategoriaDAO categoriaDAO;
	
	@GetMapping("/patrimonio")
	public String abrirPatrimonioIndex() {
		return "patrimonio/index-patrimonio";
	}
	
	@GetMapping("/patrimonio/lista")
	public String abrirPatrimonioLista(Model model) {
		model.addAttribute("patrimonios", patrimonioDAO.buscarTodos());
		return "patrimonio/lista";
	}
	
	@GetMapping("/adm/patrimonio/novo")
	public String abrirCadastroPatrimonio(Model model) {
		model.addAttribute("patrimonio", new Patrimonio());
		model.addAttribute("categorias", categoriaDAO.buscarTodas());
		return "patrimonio/form";
	}
	
	@PostMapping("/adm/patrimonio/salvar")
	public String salvar(@Valid Patrimonio patrimonio, BindingResult brPatrimonio, Model model, HttpSession session, @RequestParam(name = "categoria.nome", required = true) String nome) {
		if (patrimonioDAO.buscarPorNome(patrimonio.getNome()) != null && patrimonioDAO.buscar(patrimonio.getId()).getCategoria().equals(nome)) {
			brPatrimonio.addError(new FieldError("patrimonio", "nome", "O patrimonio '"+patrimonio.getNome()+"' j� est� cadastrado. Voc� pode alterar sua categoria atual"));
		}
		if (brPatrimonio.hasErrors()) {
			System.out.println("Erros ocorreram: ");
			System.out.println(brPatrimonio);
			System.out.println("**************************************************************************************************************");
			model.addAttribute("categorias", categoriaDAO.buscarTodas());
			return "patrimonio/form";
		}
		Usuario usuarioAutenticado = (Usuario) session.getAttribute("usuarioAutenticado");
		if (patrimonio.getId() == null) {
			patrimonio.setDataCadastro(new Date());
			patrimonio.setUsuario(usuarioAutenticado);
			patrimonioDAO.persistir(patrimonio);
			return "redirect:/app/adm/patrimonio/novo";
		}
		if (patrimonio.getUsuario() == null) {
			patrimonio.setUsuario(usuarioAutenticado);
		}
		if(patrimonioDAO.buscar(patrimonio.getId()).getCategoria().equals(nome)) {
			brPatrimonio.addError(new FieldError("patrimonio", "nome", "Para atualizar o patrim�nio, o ambiente deve ser diferente"));
		}
			patrimonioDAO.alterar(patrimonio);
			return "redirect:/app/patrimonio/lista";
	}
	
	@GetMapping("/adm/patrimonio/editar")
	public String abrirEditarPatrimonio(@RequestParam(name = "id", required = true) Long id, Patrimonio patrimonio, Model model) {
		Patrimonio patrimonioBuscado = patrimonioDAO.buscar(id);
		model.addAttribute("patrimonio", patrimonioBuscado);
		model.addAttribute("categorias", categoriaDAO.buscarTodas());
//		model.addAttribute("patrimonios", patrimonioDAO.buscarTodos());
		return "patrimonio/form";
	}
	
	@GetMapping("/adm/patrimonio/deletar")
	public String deletar(@RequestParam(name = "id", required = true) Long id) {
		patrimonioDAO.deletar(patrimonioDAO.buscar(id));
		return "redirect:/app/patrimonio/lista";
	}
}