package br.senai.sp.informatica.pweb.senaipkeeper.controllers;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.senai.sp.informatica.pweb.senaipkeeper.dao.CategoriaDAO;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Categoria;

@Controller
@RequestMapping("/app")
public class CategoriaController {
	
	@Autowired
	private CategoriaDAO categoriaDAO;
	
	@GetMapping("/categoria")
	public String abrirCategoriaIndex() {
		return "categoria/index-categoria";
	}	
	
	@GetMapping("/categoria/lista")
	public String abrirCategoriaLista(Model model) {
		model.addAttribute("categorias", categoriaDAO.buscarTodas());
		return "categoria/lista";
	}
	
	@GetMapping("/adm/categoria/nova")
	public String abrirCadastroCategoria(Model model) {
		model.addAttribute("categoria", new Categoria());
		return "categoria/form";
	}
	
	@PostMapping("/adm/categoria/salvar")
	public String salvar(@Valid Categoria categoria, BindingResult brCategoria, Model model, HttpSession session) {
		if (categoriaDAO.buscarPorNome(categoria.getNome()) != null) {
			brCategoria.addError(new FieldError("categoria", "nome", "O categoria '"+categoria.getNome()+"' j� est� cadastrado no sistema."));
		}
		
		if (brCategoria.hasErrors()) {
			System.out.println("Erros ocorreram: ");
			System.out.println(brCategoria);
			System.out.println("**********************************************************************************************************");
			return "categoria/form";
		}
		
		if (categoria.getId() == null) {
			categoriaDAO.persistir(categoria);
			return "redirect:/app/adm/categoria/nova";
		}
			categoriaDAO.alterar(categoria);
			return "redirect:/app/categoria/lista";
	}
	
	@GetMapping("/adm/categoria/editar")
	public String abrirEditarCategoria(@RequestParam(name = "id", required = true) Long id, Model model, Categoria categoria) {
		Categoria categoriaBuscado = categoriaDAO.buscar(id);
		model.addAttribute("categoria", categoriaBuscado);
		model.addAttribute("categorias", categoriaDAO.buscarTodas());
		return "categoria/form";
	}
	
	@GetMapping("/adm/categoria/deletar")
	public String deletar(@RequestParam(name = "id", required = true) Long id) {
		categoriaDAO.deletar(categoriaDAO.buscar(id));
		return "redirect:/app/categoria/lista";
	}
}