package br.senai.sp.informatica.pweb.senaipkeeper.controllers;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.DigestUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.senai.sp.informatica.pweb.senaipkeeper.dao.UsuarioDAO;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Usuario;

@Controller
@RequestMapping("/app")
public class UsuarioController {
	
	@Autowired
	private UsuarioDAO usuarioDAO;
	
	@GetMapping({"/", ""})
	public String abrirHomeUsuario() {
		return "funcionalidades/home";
	}
	
	@GetMapping("/usuario/alterarSenha")
	public String abrirFormAlterarSenha(Model model) {
		model.addAttribute("usuario", new Usuario());
		return "usuario/alterarSenha";
	}
	
	@PostMapping(value = "/usuario/senha")
	public String editarSenha(@Valid Usuario usuario, BindingResult brSenha, HttpSession session) {
		
		if (brSenha.hasFieldErrors("senhaAntiga") || brSenha.hasFieldErrors("senha") || brSenha.hasFieldErrors("confirmaSenha")) {
			return "usuario/alterarSenha";
		}
		// Pegando o usu�rio da sess�o e logo ap�s, buscando o mesmo no banco
		Usuario usuarioSessao = (Usuario) session.getAttribute("usuarioAutenticado");
		Usuario usuarioAutenticado = usuarioDAO.buscar(usuarioSessao.getId());
		if (!DigestUtils.md5DigestAsHex(usuario.getSenhaAntiga().getBytes()).equals(usuarioAutenticado.getSenha())) {
			brSenha.addError(new FieldError("usuario", "senhaAntiga", "A senha antiga est� errada"));
			return "usuario/alterarSenha";
		}
		if (!usuario.getSenha().equals(usuario.getConfirmaSenha())) {
			brSenha.addError(new FieldError("usuario", "confirmaSenha", "A senha nova e sua confirma��o s�o divergentes"));
			return "usuario/alterarSenha";
		}
		if (DigestUtils.md5DigestAsHex(usuario.getSenhaAntiga().getBytes()).equals(DigestUtils.md5DigestAsHex(usuario.getSenha().getBytes()))) {
			brSenha.addError(new FieldError("usuario", "senha", "Sua senha n�o pode ser igual a antiga"));
			return "usuario/alterarSenha";
		}
		
		usuarioAutenticado.setSenha(usuario.getSenha());
		usuarioAutenticado.senhaHash();
		usuario.senhaHash();
		usuarioDAO.alterar(usuarioAutenticado);
		
		return "redirect:/app";
	}
	
	@GetMapping("/usuario")
	public String abrirUsuarioIndex() {
		return "usuario/index-usuario";
	}
	
	@GetMapping("/perfil")
	public String abrirPerfilEdicao() {
		return "usuario/form";
	}
}