package br.senai.sp.informatica.pweb.senaipkeeper.controllers;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.senai.sp.informatica.pweb.senaipkeeper.dao.AmbienteDAO;
import br.senai.sp.informatica.pweb.senaipkeeper.dao.ItemPatrimonioDAO;
import br.senai.sp.informatica.pweb.senaipkeeper.dao.MovimentacaoDAO;
import br.senai.sp.informatica.pweb.senaipkeeper.dao.PatrimonioDAO;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Ambiente;
import br.senai.sp.informatica.pweb.senaipkeeper.models.ItemPatrimonio;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Movimentacao;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Usuario;

@Controller
@RequestMapping("/app")
public class MovimentacaoController {

	@Autowired
	private MovimentacaoDAO movimentacaoDAO;
	
	@Autowired
	private ItemPatrimonioDAO itemPatrimonioDAO;
	
	@Autowired
	private PatrimonioDAO patrimonioDAO;
	
	@Autowired
	private AmbienteDAO ambienteDAO;
	
	@GetMapping("/movimentacao")
	public String abrirMovimentacao() {
		return "movimentacao/index-movimentacao";
	}
	
	@GetMapping("/movimentacao/lista")
	public String abrirListaMovimentacao(Model model) {
		model.addAttribute("movimentacoes", movimentacaoDAO.buscarTodos());
		return "movimentacao/lista";
	}
	
	@GetMapping("/movimentacao/novo")
	public String abrirCadastroMovimentacao(Model model) {
//		model.addAttribute("movimentacao", new Movimentacao());
//		model.addAttribute("itempatrimonios", itemPatrimonioDAO.buscarTodos());
//		model.addAttribute("patrimonios", patrimonioDAO.buscarTodos());
//		model.addAttribute("ambientes", ambienteDAO.buscarTodos());
		System.out.println("Entrei no novo");
		return "movimentacao/form";
	}
	
	@PostMapping("/movimentacao/salvar")
	public String salvar(Movimentacao movimentacao, Model model, HttpSession session) {
		System.out.println("Entrei no salvar");
		if (movimentacao.getId() == null) {
			
			Usuario usuarioAutenticado = (Usuario) session.getAttribute("usuarioAutenticado");
			movimentacao.setUsuario(usuarioAutenticado);
			movimentacao.setDataMovimentacao(new Date());
			ItemPatrimonio ambienteMovimentado = itemPatrimonioDAO.buscar(movimentacao.getItemPatrimonio().getId());
			Ambiente destino = ambienteDAO.buscarPorNome(movimentacao.getAmbiente_destino().getNome());
			ambienteMovimentado.setAmbiente(destino);
//			Ambiente novoAmbiente = ambienteDAO.buscarPorNome(movimentacao.getAmbiente_destino().getNome());
			Ambiente novoAmbiente = destino;
			ambienteMovimentado.setAmbiente(novoAmbiente);
			
			ambienteMovimentado.setUltimaMovimentacao(movimentacao.getDataMovimentacao());
			movimentacao.setAmbiente_origem(ambienteDAO.buscarPorNome(movimentacao.getAmbiente_origem().getNome()));
			movimentacao.setAmbiente_destino(destino);
			itemPatrimonioDAO.alterar(ambienteMovimentado);
			
			System.out.println(movimentacao);
			
			movimentacaoDAO.persistir(movimentacao);
		}
//		return "redirect:/app/movimentacao/lista";
		return "redirect:/app/movimentacao/lista";
	}
	
}