package br.senai.sp.informatica.pweb.senaipkeeper.controllers;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.senai.sp.informatica.pweb.senaipkeeper.utils.EmailUtils;
import br.senai.sp.informatica.pweb.senaipkeeper.dao.UsuarioDAO;
import br.senai.sp.informatica.pweb.senaipkeeper.models.TiposUsuarios;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Usuario;

@Controller
@RequestMapping("/app/adm")
public class AdministradorController {
	
	@Autowired
	private UsuarioDAO usuarioDAO;
	
	@GetMapping("/usuario")
	public String abrirListaUsuarios(Model model) {
		model.addAttribute("usuarios", usuarioDAO.buscarTodos());
		return "usuario/lista";
	}
	
	@GetMapping("/usuario/novo")
	public String abrirFormNovoUsuario(Model model) {
		model.addAttribute("usuario", new Usuario());
		return "usuario/form";
	}
	
	@GetMapping("/usuario/editar")
	public String abrirEdicao(Model model, @RequestParam(name = "id", required = true) Long id, HttpServletResponse response) throws IOException{
		Usuario usuarioBuscado = usuarioDAO.buscar(id);
		model.addAttribute("usuario", usuarioBuscado);
		return "usuario/form";
	}

	@PostMapping(value = "/usuario/salvar")
	public String salvar(@Valid Usuario usuario, BindingResult brUsuario, @RequestParam(name = "confirmacaoSenha",
	required = false) String confirmacao, @RequestParam(name = "administrador", required = false) Boolean isAdministrador) {
		// Checar o cadastro
		if (usuario.getId() == null) {
			// Senhas diferentes
			if (!confirmacao.equals(usuario.getSenha())) {
				brUsuario.addError(new FieldError("usuario", "senha", "a senha est� incorreta")); // Objeto / campo / mensagem
			}
			// Email existente
			if (usuarioDAO.buscarPorEmail(usuario.getEmail()) != null) {
				brUsuario.addError(new FieldError("usuario", "email", "Esse email '"+usuario.getEmail()+"' j� est� sendo usado"));
			}
			// BindingResult cont�m erros?
			if (brUsuario.hasErrors()) {
				System.out.println("**********************************************************");
				System.out.println("******************** Ocorreram erros. ********************");
				System.out.println("**********************************************************");
				System.out.println(brUsuario);
				return "usuario/form";
			}
		}
		// � uma altera��o?
		if (brUsuario.hasFieldErrors("nome") || brUsuario.hasFieldErrors("sobrenome")) {
			return "usuario/form";			
		}
		
		// Verifica a autoridade do usu�rio
		if (isAdministrador != null) {
			usuario.setTipo(TiposUsuarios.ADMINISTRADOR);
		}else {
			usuario.setTipo(TiposUsuarios.COMUM);
		}
		
		if (usuario.getId() == null) {
			usuario.senhaHash();
			usuarioDAO.persistir(usuario);
			String titulo = "Gerenciamento de patrim�nios SENAI";
			String corpo = "Ol�, " + usuario.getNome() + " " + usuario.getSobrenome() + ", voc� foi cadastrado no sistema de gerenciamento de patrim�nios do SENAI.\n"
					+ "\nA sua senha �: " + confirmacao + " e voc� pode alter�-la a qualquer momento.";
			try {
				EmailUtils.enviarMensagem(titulo, corpo, usuario.getEmail());
			} catch (MessagingException e) {
				
				e.printStackTrace();
			}
		}
		Usuario usuarioExistente = usuarioDAO.buscar(usuario.getId());
		usuarioExistente.setNome(usuario.getNome());
		usuarioExistente.setSobrenome(usuario.getSobrenome());
		usuarioDAO.alterar(usuarioExistente);
		return "redirect:/app";
	}
	
	@GetMapping("/usuario/deletar")
	public String deletar(@RequestParam(required = true) Long id, HttpServletResponse response) throws IOException{
		usuarioDAO.deletar(usuarioDAO.buscar(id));
		return "redirect:/app/adm/usuario";
	}
}