package br.senai.sp.informatica.pweb.senaipkeeper.controllers;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import br.senai.sp.informatica.pweb.senaipkeeper.dao.UsuarioDAO;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Usuario;

@Controller
public class IndexController {
	
	@Autowired 
	private UsuarioDAO usuarioDAO;

	@GetMapping(value = {"/", ""})
	public String abrirIndex(Model model, HttpSession session) {
		if (session.getAttribute("usuarioAutenticacao") != null) {
			return "redirect:/app";
		}
		model.addAttribute("usuario", new Usuario());
		return "index";
	}
	
	@PostMapping("/usuario/autenticar")
	public String autenticar(@Valid Usuario usuario, BindingResult brUsuario, HttpSession session, Model model) {
		// Verificando se h� erros, caso sim, retornar� ao index
		if (brUsuario.hasFieldErrors("email") || brUsuario.hasFieldErrors("senha")) {
			System.out.println("**********************************************************");
			System.out.println("******************** Ocorreram erros. ********************");
			System.out.println("**********************************************************");
			System.out.println(brUsuario);
			return "index";
		}
		// Faz o hash do campo senha para bater com o resultado j� hasheado do banco
		usuario.senhaHash();
		/* Vai ao banco e busca ambos email e senha, caso um deles esteja errado, o usu�rio retorna ao index
		 * caso tudo esteja correto, ele ir� "pendurar" o usu�rio na sess�o*/
		Usuario usuarioAutenticado = usuarioDAO.buscarPorEmailESenha(usuario.getEmail(), usuario.getSenha());
		if (usuarioAutenticado == null) {
			brUsuario.addError(new FieldError("usuario", "email", "E-mail ou senha inv�lidos"));
			return "index";
		}
		session.setAttribute("usuarioAutenticado", usuarioAutenticado);
		model.addAttribute("usuario", usuarioAutenticado);
		return "redirect:/app/";
	}
	
	@GetMapping({"/sair"})
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:/";
	}
}