package br.senai.sp.informatica.pweb.senaipkeeper.controllers;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.senai.sp.informatica.pweb.senaipkeeper.dao.AmbienteDAO;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Ambiente;

@Controller
@RequestMapping("/app")
public class AmbienteController {

	@Autowired
	private AmbienteDAO ambienteDAO;
	
	@GetMapping("/ambiente")
	public String abrirAmbienteIndex() {
		return "ambiente/index-ambiente";
	}
	
	@GetMapping("/ambiente/lista")
	public String abrirAmbienteLista(Model model) {
		model.addAttribute("ambientes", ambienteDAO.buscarTodos());
		return "ambiente/lista";
	}
	
	@GetMapping("/adm/ambiente/nova")
	public String abrirCadastroAmbiente(Model model) {
		model.addAttribute("ambiente", new Ambiente());
		return "ambiente/form";
	}
	
	@PostMapping("/adm/ambiente/salvar")
	public String salvar(@Valid Ambiente ambiente, BindingResult brAmbiente, Model model, HttpSession session) {
		if (ambienteDAO.buscarPorNome(ambiente.getNome()) != null) {
			brAmbiente.addError(new FieldError("ambiente", "nome", "O ambiente '"+ambiente.getNome()+"' j� est� cadastrado no sistema."));
		}
		
		if (brAmbiente.hasErrors()) {
			System.out.println("Erros ocorreram: ");
			System.out.println(brAmbiente);
			System.out.println("**********************************************************************************************************");
			return "ambiente/form";
		}
		
		if (ambiente.getId() == null) {
			ambienteDAO.persistir(ambiente);
			return "redirect:/app/adm/ambiente/nova";
		}
			ambienteDAO.alterar(ambiente);
		return "redirect:/app/ambiente/lista";
	}
	
	@GetMapping("/adm/ambiente/editar")
	public String abrirEditarAmbiente(@RequestParam(name = "id", required = true) Long id, Model model, Ambiente ambiente) {
		Ambiente ambienteBuscado = ambienteDAO.buscar(id);
		model.addAttribute("ambiente", ambienteBuscado);
		model.addAttribute("ambientes", ambienteDAO.buscarTodos());
		return "ambiente/form";
	}
	
	@GetMapping("/adm/ambiente/deletar")
	public String deletar(@RequestParam(name = "id", required = true) Long id) {
		
		ambienteDAO.deletar(ambienteDAO.buscar(id));
		return "redirect:/app/ambiente/lista";
	}
}