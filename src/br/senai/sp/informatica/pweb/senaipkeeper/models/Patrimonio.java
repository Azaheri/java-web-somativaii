package br.senai.sp.informatica.pweb.senaipkeeper.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "patrimonio")
//@OnDelete(action = OnDeleteAction.CASCADE)
public class Patrimonio {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@OnDelete(action = OnDeleteAction.CASCADE)
	private Long id;
	
	@NotNull
	@Size(min = 1, max = 40)
	@Column(length = 40, nullable = false, unique = true)
	private String nome;
	
	@ManyToOne
//	@OnDelete(action = OnDeleteAction.CASCADE)
	@NotNull
	@JoinColumn(name = "categoria_id", nullable = false)
	private Categoria categoria;
	
	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "usuario_id", nullable = false)
	private Usuario usuario;
	
	@Column
	@JsonFormat(pattern = "dd 'de' MMMM 'de' yyyy '�s' kk:mm:ss")
	private Date dataCadastro;

	@Override
	public String toString() {
		return "\nPatrimonio: " + nome;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
}