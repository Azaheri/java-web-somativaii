package br.senai.sp.informatica.pweb.senaipkeeper.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "item_patrimonio")
//@OnDelete(action = OnDeleteAction.CASCADE)
public class ItemPatrimonio {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@OnDelete(action = OnDeleteAction.CASCADE)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "patrimonio_id", nullable = false)
//	@OnDelete(action = OnDeleteAction.CASCADE)
	private Patrimonio patrimonio;
	
	@ManyToOne
	@JoinColumn(name = "ambiente_id", nullable = false)
//	@OnDelete(action = OnDeleteAction.CASCADE)
	private Ambiente ambiente;
	
	@ManyToOne
	@JoinColumn(name = "usuario_id", nullable = false)
//	@OnDelete(action = OnDeleteAction.CASCADE)
	private Usuario usuario;

	@JsonFormat(pattern = "dd 'de' MMMM 'de' yyyy '�s' kk:mm:ss")
	private Date ultimaMovimentacao;
	
	public Date getUltimaMovimentacao() {
		return ultimaMovimentacao;
	}

	public void setUltimaMovimentacao(Date ultimaMovimentacao) {
		this.ultimaMovimentacao = ultimaMovimentacao;
	}

//	@Override
//	public String toString() {
//		return "\nPatrimonio: " + patrimonio.getNome() + "\nLocal atual: " + ambiente.getNome() + "\nUsuario que registrou: "
//				+ usuario.getNome() + " " + usuario.getSobrenome();
//	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Patrimonio getPatrimonio() {
		return patrimonio;
	}

	public void setPatrimonio(Patrimonio patrimonio) {
		this.patrimonio = patrimonio;
	}

	public Ambiente getAmbiente() {
		return ambiente;
	}

	public void setAmbiente(Ambiente ambiente) {
		this.ambiente = ambiente;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}