package br.senai.sp.informatica.pweb.senaipkeeper.models;

public enum TiposUsuarios {
	ADMINISTRADOR, COMUM
}
