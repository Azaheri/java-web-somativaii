package br.senai.sp.informatica.pweb.senaipkeeper.models;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.util.DigestUtils;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "usuario")
//@OnDelete(action = OnDeleteAction.CASCADE)
@JsonIgnoreProperties({"senhaAntiga", "confirmaSenha"})
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)

public class Usuario extends UsuarioBase
//implements Authentication
{

//	// Determina que � um id
//	@Id
//	// Determina que o id ir� auto incrementar
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	private Long id;
//	
//	// Determina que o campo n�o poder� ser nulo
//	@NotNull
//	// Determina o tamanho que o campo dever� possuir
//	@Size(min = 1, max = 20)
//	// Informa��o sobre a coluna que ser� usada para cria��o no banco
//	@Column(length = 20, nullable = false, unique = false)
//	private String nome;
	
	@NotNull
	@Size(min = 1, max = 40)
	@Column(length = 40, nullable = false, unique = false)
	private String sobrenome;
	
	@NotNull
	@Size(min = 1, max = 100)
	// Determina que o email dever ser v�lido para prosseguir o cadastro
	@Email
	@Column(length = 100, nullable = false, unique = true)
	private String email;
	
	@NotNull
	@Size(min = 6, max = 32)
	@JsonIgnore
	@JsonInclude
	@Column(length = 32, nullable = false, unique = false)
	private String senha;
	
	@NotNull
	private TiposUsuarios tipo = TiposUsuarios.COMUM;
	
	@Transient // Significa que n�o participa da persistencia no banco
	//@JsonIgnore
	private String senhaAntiga;
	
	@Transient
	//@JsonIgnore
	private String confirmaSenha;
	
//	public Long getId() {
//		return id;
//	}
//
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	public String getNome() {
//		return nome;
//	}
//
//	public void setNome(String nome) {
//		this.nome = nome;
//	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	@JsonProperty
	public void setSenha(String senha) {
		this.senha = senha;
	}

	public TiposUsuarios getTipo() {
		return tipo;
	}

	public void setTipo(TiposUsuarios tipo) {
		this.tipo = tipo;
	}

	public String getSenhaAntiga() {
		return senhaAntiga;
	}

	public void setSenhaAntiga(String senhaAntiga) {
		this.senhaAntiga = senhaAntiga;
	}

	public String getConfirmaSenha() {
		return confirmaSenha;
	}

	public void setConfirmaSenha(String confirmaSenha) {
		this.confirmaSenha = confirmaSenha;
	}

	// Hash de senhas do usu�rio
	public void senhaHash() {
		this.senha = DigestUtils.md5DigestAsHex(this.senha.getBytes());
	}
	
	// Usu�rio como administrador
	//@JsonIgnore
	public boolean getAdministrador() {
		return this.tipo.equals(TiposUsuarios.ADMINISTRADOR);
	}
//	
//// A partir daqui, entra a parte do JWT
//	@Override
//	//@JsonIgnore
//	public String getName() 
//	{
////		return nome;
//	}
//
//	@Override
//	//@JsonIgnore
//	public Collection<? extends GrantedAuthority> getAuthorities() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	//@JsonIgnore
//	public Object getCredentials() 
//	{
//		return null;
//	}
//
//	@Override
//	//@JsonIgnore
//	public Object getDetails() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	//@JsonIgnore
//	public Object getPrincipal() 
//	{
////		return this;
//	}
//
//	@Override
////	@JsonIgnore
//	public boolean isAuthenticated()
//	{
//		return true;
//	}
//
//	@Override
////	@JsonIgnore
//	public void setAuthenticated(boolean arg0) throws IllegalArgumentException {
//		// TODO Auto-generated method stub
//		
//	}

	@Override
	public String toString() {
		return "Usuario [sobrenome=" + sobrenome + ", email=" + email + ", senha=" + senha + ", tipo=" + tipo
				+ ", senhaAntiga=" + senhaAntiga + ", confirmaSenha=" + confirmaSenha + "]";
	}

//	@Override
//	public String toString() {
//		return "Usuario [id=" + id + ", nome=" + nome + ", sobrenome=" + sobrenome + ", email=" + email + ", senha="
//				+ senha + ", tipo=" + tipo + ", senhaAntiga=" + senhaAntiga + ", confirmaSenha=" + confirmaSenha + "]";
//	}
//	

	
	
}