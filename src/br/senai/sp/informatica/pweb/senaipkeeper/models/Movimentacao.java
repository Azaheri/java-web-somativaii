package br.senai.sp.informatica.pweb.senaipkeeper.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "movimentacao")
public class Movimentacao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
//	@OnDelete(action = OnDeleteAction.CASCADE)
	@NotNull
	@JoinColumn(name = "ambiente_origem_id", nullable = false)
	private Ambiente ambiente_origem;

	@ManyToOne
//	@OnDelete(action = OnDeleteAction.CASCADE)
	@NotNull
	@JoinColumn(name = "ambiente_destino_id", nullable = false)
	private Ambiente ambiente_destino;
	
	@ManyToOne
//	@OnDelete(action = OnDeleteAction.CASCADE)
	@NotNull
	@JoinColumn(name = "usuario_id", nullable = false)
	private Usuario usuario;
	
	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	@NotNull
	@JoinColumn(name = "item_patrimonio_id", nullable = false)
	private ItemPatrimonio itemPatrimonio;

	@Column(nullable = false)
	@JsonFormat(pattern = "dd 'de' MMMM 'de' yyyy '�s' kk:mm:ss")
	private Date dataMovimentacao;

	
	
	
	@Override
	public String toString() {
		return "Movimentacao [id=" + id + ", ambiente_origem=" + ambiente_origem + ", ambiente_destino="
				+ ambiente_destino + ", usuario=" + usuario + ", itemPatrimonio=" + itemPatrimonio
				+ ", dataMovimentacao=" + dataMovimentacao + "]";
	}
	
	
	
	
	
	
	
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Ambiente getAmbiente_origem() {
		return ambiente_origem;
	}

	public void setAmbiente_origem(Ambiente ambiente_origem) {
		this.ambiente_origem = ambiente_origem;
	}

	public Ambiente getAmbiente_destino() {
		return ambiente_destino;
	}

	public void setAmbiente_destino(Ambiente ambiente_destino) {
		this.ambiente_destino = ambiente_destino;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public ItemPatrimonio getItemPatrimonio() {
		return itemPatrimonio;
	}

	public void setItemPatrimonio(ItemPatrimonio itemPatrimonio) {
		this.itemPatrimonio = itemPatrimonio;
	}

	public Date getDataMovimentacao() {
		return dataMovimentacao;
	}

	public void setDataMovimentacao(Date dataMovimentacao) {
		this.dataMovimentacao = dataMovimentacao;
	}

//	@Override
//	public String toString() {
//		return "Movimentacao [ambiente_origem=" + ambiente_origem + ", ambiente_destino=" + ambiente_destino
//				+ ", usuario=" + usuario + ", itemPatrimonio=" + itemPatrimonio + ", dataMovimentacao="
//				+ dataMovimentacao + "]";
//	}
}