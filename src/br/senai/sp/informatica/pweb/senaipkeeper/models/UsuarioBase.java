package br.senai.sp.informatica.pweb.senaipkeeper.models;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

@MappedSuperclass
public abstract class UsuarioBase{
	// Determina que � um id
	@Id
	// Determina que o id ir� auto incrementar
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	// Determina que o campo n�o poder� ser nulo
	@NotNull
	// Determina o tamanho que o campo dever� possuir
	@Size(min = 1, max = 20)
	// Informa��o sobre a coluna que ser� usada para cria��o no banco
	@Column(length = 20, nullable = false, unique = false)
	private String nome;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "UsuarioBase [id=" + id + ", nome=" + nome + "]";
	}


}