package br.senai.sp.informatica.pweb.senaipkeeper.exceptions;

/**
 * Exception destinada a solicita��es de entidades n�o cadastradas (404)
 * @author 39270271897 - Matheus Jos� Felipe Rosa
 *
 */
public class EntidadeNaoEncontrada extends Exception{

}