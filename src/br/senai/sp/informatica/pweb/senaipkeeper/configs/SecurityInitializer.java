package br.senai.sp.informatica.pweb.senaipkeeper.configs;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
import org.springframework.stereotype.Component;

@Component
public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer{}
