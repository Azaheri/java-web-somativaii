package br.senai.sp.informatica.pweb.senaipkeeper.configs;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import com.auth0.jwt.exceptions.JWTVerificationException;

import br.senai.sp.informatica.pweb.senaipkeeper.models.Usuario;
import br.senai.sp.informatica.pweb.senaipkeeper.models.UsuarioJWT;
import br.senai.sp.informatica.pweb.senaipkeeper.utils.JwtUtils;

@Component
public class JwtFilter extends GenericFilterBean 
{

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException 
	{
		HttpServletRequest request = (HttpServletRequest) req;
		String authorization = request.getHeader("Authorization");
		
		if (authorization != null)
		{
			
			if (authorization.matches("(Bearer )(\\w+|\\.|-)+"))
			{
				String token = authorization.split(" ")[1];
				
				try 
				{
					JwtUtils.validarToken(token);
					UsuarioJWT usuarioToken = (UsuarioJWT) JwtUtils.extrairUsuarioDoToken(token);
					SecurityContextHolder.getContext().setAuthentication(usuarioToken);
				}
				
				catch (JWTVerificationException e) 
				{
					System.out.println("01");
					e.printStackTrace();
				}
				
				catch (IllegalArgumentException e) 
				{
					System.out.println("02");
					e.printStackTrace();
				}
				
				catch (Exception e) 
				{
					System.out.println("03");
					e.printStackTrace();
				}
				
				System.out.println(token);
			}
			
			else
			{
				System.out.println("Token inv�lido");
			}
		}
		
		else
		{
			System.out.println("O Authorization n�o foi informado");			
		}
		
		chain.doFilter(req, res);
	}
}