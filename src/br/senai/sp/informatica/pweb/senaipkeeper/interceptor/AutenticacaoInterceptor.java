package br.senai.sp.informatica.pweb.senaipkeeper.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import br.senai.sp.informatica.pweb.senaipkeeper.models.TiposUsuarios;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Usuario;

@Component
public class AutenticacaoInterceptor extends HandlerInterceptorAdapter{
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		boolean necessitaAutenticacao = request.getRequestURI().contains("/app");
		boolean necessitaAdm = request.getRequestURI().contains("/adm");
		Usuario usuarioAutenticado = (Usuario) request.getSession().getAttribute("usuarioAutenticado");
		boolean usuarioEstaLogado = usuarioAutenticado != null;
		
		if (necessitaAutenticacao && !usuarioEstaLogado) {
			return false;
		}else if(necessitaAdm && usuarioAutenticado.getTipo() != TiposUsuarios.ADMINISTRADOR) {
			return false;
		}
		return true;
	}
}