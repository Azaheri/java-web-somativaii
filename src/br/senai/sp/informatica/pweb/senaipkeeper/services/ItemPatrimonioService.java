package br.senai.sp.informatica.pweb.senaipkeeper.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import br.senai.sp.informatica.pweb.senaipkeeper.dao.AmbienteDAO;
import br.senai.sp.informatica.pweb.senaipkeeper.dao.ItemPatrimonioDAO;
import br.senai.sp.informatica.pweb.senaipkeeper.dao.MovimentacaoDAO;
import br.senai.sp.informatica.pweb.senaipkeeper.dao.UsuarioDAO;
import br.senai.sp.informatica.pweb.senaipkeeper.exceptions.EntidadeNaoEncontrada;
import br.senai.sp.informatica.pweb.senaipkeeper.exceptions.ValidacaoException;
import br.senai.sp.informatica.pweb.senaipkeeper.models.ItemPatrimonio;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Movimentacao;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Usuario;
import br.senai.sp.informatica.pweb.senaipkeeper.models.UsuarioBase;
import br.senai.sp.informatica.pweb.senaipkeeper.models.UsuarioJWT;

@Service
public class ItemPatrimonioService 
{
	@Autowired
	private ItemPatrimonioDAO itemPatrimonioDAO;
	
	@Autowired
	private MovimentacaoDAO movimentacaoDAO;
	
	@Autowired
	private AmbienteDAO ambienteDAO;
	
	@Autowired
	private UsuarioDAO usuarioDAO;
	
	// /rest/itens
	public List<ItemPatrimonio> buscarTodos()
	{		
		return itemPatrimonioDAO.buscarTodos();
	}
	
	// /rest/itens/{id}
	public ItemPatrimonio buscar(Long id) throws EntidadeNaoEncontrada
	{
		ItemPatrimonio itemPatrimonioDB = itemPatrimonioDAO.buscar(id);
		if (itemPatrimonioDB == null)
		{
			throw new EntidadeNaoEncontrada();
		}
		return itemPatrimonioDB;
	}
	
	// /rest/itens/{id}/movimentacoes <-- Get
	public Movimentacao movimentacoes(Long id) throws EntidadeNaoEncontrada
	{
		return movimentacaoDAO.movimentacoesDeItens(buscar(id));
	}
	
	// /rest/itens/{id}/movimentacoes  <-- Post
	public Movimentacao movimentarItem(Long id, Movimentacao movimentacao, BindingResult bindingResult) throws EntidadeNaoEncontrada, ValidacaoException 
	{
		if (buscar(id).getId() == null || !(buscar(id).getId().equals(id))) {
			throw new EntidadeNaoEncontrada();
		}
		
		if (buscar(id).getUltimaMovimentacao() != null) {
			
		}
		UsuarioJWT usuarioJWT= (UsuarioJWT) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		Usuario userAuth = new Usuario();
		userAuth.setId(usuarioJWT.getId());
		
//		Usuario us = (Usuario) usuario;
		
//		Usuario u = Usuario
		
//		movimentacao.setUsuario((Usuario) usuario.getPrincipal());
		movimentacao.setUsuario(userAuth);
		movimentacao.setAmbiente_origem(buscar(id).getAmbiente());
		movimentacao.setDataMovimentacao(new Date());
		movimentacao.setItemPatrimonio(buscar(id));
		movimentacao.setAmbiente_destino(movimentacao.getAmbiente_destino());
		ItemPatrimonio itemDB = buscar(id);
		itemDB.setAmbiente(movimentacao.getAmbiente_destino());
		itemDB.setUltimaMovimentacao(movimentacao.getDataMovimentacao());
		
		if (movimentacao.getAmbiente_destino() == null || itemDB.getAmbiente() == null
				|| itemDB.getAmbiente().getId() == null || movimentacao.getAmbiente_destino().getId() == null
				|| ambienteDAO.buscar(movimentacao.getAmbiente_destino().getId()) == null) {
			throw new EntidadeNaoEncontrada();
		}
		
		if (buscar(id).getAmbiente().getId() == movimentacao.getAmbiente_destino().getId()) 
		{
			bindingResult.addError(new FieldError("movimentacao", "ambiente_destino", "O item j� est� neste ambiente"));
			throw new ValidacaoException();
		}
		
		itemPatrimonioDAO.alterar(itemDB);
		movimentacaoDAO.persistir(movimentacao);
		return movimentacao;
	}
}