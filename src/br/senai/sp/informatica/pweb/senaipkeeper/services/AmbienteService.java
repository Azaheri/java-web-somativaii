package br.senai.sp.informatica.pweb.senaipkeeper.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.senai.sp.informatica.pweb.senaipkeeper.dao.AmbienteDAO;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Ambiente;

@Service
public class AmbienteService 
{
	@Autowired
	private AmbienteDAO ambienteDAO;
	
	// /rest/ambientes
	public List<Ambiente> buscarTodos()
	{
		return ambienteDAO.buscarTodos();
	}
}
