package br.senai.sp.informatica.pweb.senaipkeeper.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import br.senai.sp.informatica.pweb.senaipkeeper.dao.UsuarioDAO;
import br.senai.sp.informatica.pweb.senaipkeeper.exceptions.EntidadeNaoEncontrada;
import br.senai.sp.informatica.pweb.senaipkeeper.exceptions.ValidacaoException;
import br.senai.sp.informatica.pweb.senaipkeeper.models.Usuario;

@Service
public class UsuarioService 
{
	@Autowired
	private UsuarioDAO usuarioDAO;
	
	// /rest/auth/jwt
	public Usuario buscarEmailESenha(Usuario usuario, BindingResult bindingResult) throws ValidacaoException, EntidadeNaoEncontrada
	{
		if (bindingResult.hasFieldErrors("email") || bindingResult.hasFieldErrors("senha")) 
		{
			throw new ValidacaoException();
		}
		
		usuario.senhaHash();
		Usuario usuarioBuscado = usuarioDAO.buscarPorEmailESenha(usuario.getEmail(), usuario.getSenha());
		
		if (usuarioBuscado == null)
		{
			throw new EntidadeNaoEncontrada();
		}
		
		return usuarioBuscado;
	}
}