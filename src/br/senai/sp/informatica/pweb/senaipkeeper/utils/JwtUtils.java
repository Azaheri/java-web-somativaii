package br.senai.sp.informatica.pweb.senaipkeeper.utils;

import java.io.UnsupportedEncodingException;
import java.util.Date;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import br.senai.sp.informatica.pweb.senaipkeeper.models.Usuario;
import br.senai.sp.informatica.pweb.senaipkeeper.models.UsuarioBase;
import br.senai.sp.informatica.pweb.senaipkeeper.models.UsuarioJWT;

public class JwtUtils 
{
	public static String gerarToken(Usuario usuario) throws IllegalArgumentException, JWTCreationException, UnsupportedEncodingException
	{
		return JWT.create()
				.withIssuer("SENAI - SENAIPKeeper")
				.withIssuedAt(new Date())
				.withSubject("Autentica��o de usu�rio")
				.withClaim("id", usuario.getId())
				.withClaim("nome", usuario.getNome())
				.sign(Algorithm.HMAC512("6fcc5e312b9129f385e8641d55f25117b46c2f3576f4f8aa43fb8e7568e9f6bb"));
	}
	
	public static UsuarioBase extrairUsuarioDoToken(String token)
	{
//		Usuario usuario = new Usuario();
		UsuarioBase usuario = new UsuarioJWT();
		DecodedJWT jwtDecodificado = JWT.decode(token);
		usuario.setId(jwtDecodificado.getClaim("id").asLong());
		usuario.setNome(jwtDecodificado.getClaim("nome").asString());
		return usuario;
	}
	
	public static void validarToken(String token) throws JWTVerificationException, IllegalArgumentException, UnsupportedEncodingException
	{
		JWT.require(Algorithm.HMAC512("6fcc5e312b9129f385e8641d55f25117b46c2f3576f4f8aa43fb8e7568e9f6bb")).build().verify(token);
	}
	
}